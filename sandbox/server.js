import https from 'https'
import express from 'express'
import React from 'react'
import { renderToString } from 'react-dom/server'
import App from './app'
import path from 'path'
import fs from 'fs'
import http from 'http'

const app = express()
const PORT = 3000

const httpsOptions = {
  key: fs.readFileSync('./sandbox/key.pem'),
  cert: fs.readFileSync('./sandbox/cert.pem')
}

app.use('/public', express.static(path.resolve(process.cwd(), './sandbox')))

app.get('/', (req, res) => {

  res.send(`
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Anue Portal Sandbox</title>
      <link rel="stylesheet" type="text/css" href="/public/style.css">
    </head>
    <body>
    <div id="app">${renderToString(<App/>)}</div>
    </body>
    <script src="/public/sandbox.bundle.js"></script>
    </html>
  `)

})

app.all('/api/*', (req, res) => {
  var proxy = http.request({
    method: req.method,
    host: 'api.beta.cnyes.cool',
    hostname: 'api.beta.cnyes.cool',
    path: req.path,
    headers: {
      ...req.headers,
      host: 'api.beta.cnyes.cool',
      origin: 'http://api.beta.cnyes.cool',
      referer: 'api.beta.cnyes.cool'
    },
  }, function(resp) {
    res.writeHead(resp.statusCode, resp.headers)
    resp.on('data', function(d) {
      res.write(d)
    })
    resp.on('end', function() {
      res.end()
    })
  })
  req.on('data', (chunk) => {
    proxy.write(chunk)
  })
  req.on('end', () => {
    proxy.end()
  })
})

https.createServer(httpsOptions, app).listen(PORT, () => {
  console.log('sandbox forwarding server listen to port ', PORT)
})

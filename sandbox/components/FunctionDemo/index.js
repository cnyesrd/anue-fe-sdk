import React, { Component } from 'react'

export default class FunctionDemo extends Component {

  state = {
    executed: false,
    error: null
  }

  render() {

    const { executed, error } = this.state
    const { onError, onClick } = this.props

    return <div className="function-demo" onClick={() => {
      this.setState({ executed: true })
      try {
        onClick()
      } catch(err) {
        this.setState({ error: err })
        onError(err.toString())
      }
    }}>
      <div className="function-demo-info">
        {this.props.text}
      </div>
      <div >
        <img className="function-check" src={`/public/imgs/${error ? 'error.svg' : 'done.svg'}`} style={{
          opacity: executed ? 1 : 0
        }}/>
      </div>
    </div>
  }
}
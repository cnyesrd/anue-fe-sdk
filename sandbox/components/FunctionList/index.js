import React, { Component } from 'react'
import FunctionDemo from '../FunctionDemo'

export default class FunctionList extends Component {

  static defaultProps = {
    functions: []
  }

  render() {
    const { functions, onError } = this.props

    return <div className="function-list">
      {functions.map(f => <FunctionDemo
          key={f.text}
          text={f.text}
          onError={onError}
          onClick={f.execute}
        />)}
    </div>
  }

}
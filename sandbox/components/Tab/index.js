import React, { Component } from 'react'

export default class Tab extends Component {

  state = {
    selected : 0
  }

  render() {

    const { tabs } = this.props
    const { selected } = this.state

    return <div className="tab">
    <div className="tabs">
    {tabs.map((t, i) => <div key={t.name} className={`${selected === i ? 'selected' : ''}`} onClick={() => this.setState({ selected: i })}>{t.name}</div>)}
    </div>
    {tabs[selected].content}
  </div> 
  }

}
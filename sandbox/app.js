import * as React from "react";
import "anue-fe-sdk";
import "anue-fe-sdk/web-preset";
import Constants from "anue-fe-sdk/Constants";
import FacebookWebProvider from 'anue-fe-sdk/FacebookWebProvider'
import AnueMailProvider from 'anue-fe-sdk/AnueMailProvider'
import GoogleWebProvider from 'anue-fe-sdk/GoogleWebProvider'
import Auth from "anue-fe-sdk/Auth";
import FunctionList from "./components/FunctionList";
import ContextView from "./components/ContextView";
import BridgeClient from "anue-fe-sdk/web-bridge-client";
import Tab from './components/Tab'

let auth, frame, client, facebook, google, mail
const host = 'https://localhost.int.cnyes.cool:9001'

if (typeof window !== "undefined") {
  google = new GoogleWebProvider({
    appId: '600742427084-7mss8ce6rn4d6mpeaibspc7j1vb2jg8g.apps.googleusercontent.com',
    apiKey: 'AIzaSyDJUflQI_CU028caIGm3zEpHSdC9gQkmOY'
  })
  facebook = new FacebookWebProvider({ appId: '591048477746530' })
  mail = new AnueMailProvider()
  auth = new Auth()
    .channel(Constants.SupportedChannel.Stock)
    .connectProvider(google, facebook, mail)
    .onStateChange((state) => {
      console.log('[sandbox] state changed %o', state)
    })
  frame = document.getElementById("portal");
  client = new BridgeClient({ target: frame });
}

class App extends React.Component {
  keyFeatures = [
  {
    text: "Visit login portal",
    execute: () => {
      location.href = `${host}?ref=sandbox`
    }
  },
  {
    text: "Visit login portal (mobile)",
    execute: () => {
      location.href = `${host}?ref=sandbox&m=1`
    }
  },
  {
    text: "Auth.getCredentialContext",
    execute: () => {
      const storedCredentials = auth.getCredentialContext();
      this.setState({ storedCredentials });
    }
  },
  {
    text: "Login (Google)",
    execute: () => {
      google.init()
      .then(p => p.login())
      .then(() => {
        const storedCredentials = auth.getCredentialContext()
        this.setState({ storedCredentials })
      })
    }
  },
  {
    text: "Refresh (Google)",
    execute: () => {
      google.refresh()
    }
  },
  {
    text: "Login (Facebook)",
    execute: () => {
      facebook.init()
      .then(p => p.login())
      .then(() => {
        const storedCredentials = auth.getCredentialContext()
        this.setState({ storedCredentials })
      })
    }
  },
  {
    text: "Auth.logout",
    execute: () => {
      auth.logout();
      const storedCredentials = auth.getCredentialContext();
      this.setState({ storedCredentials });
    }
  }]
  functions = [
    {
      text: 'Register Google state change handler',
      execute: () => {
        google
        .init()
        .then(() => {
          console.log('[sandbox] login => ', google.isLogin())
          google.onLoginStateChange((ctx) => {
            console.log('[sandbox] status changed', ctx)
          })
        })
      }
    },{
      text: 'Google logout (main window)',
      execute: () => {
        google
        .init()
        .then(() => {
          google.logout()
        })
      }
    },
    {
      text: 'GoogleWebProvider refresh token (main window)',
      execute: () => {
        try {
          google
          .init()
          .then(() => {
            if(google.isLogin())
              return google.refresh()
            else
              return google.login()
          })
          .then((ctx) => {
            console.log('[sandbox] refreshed ctx', ctx)
          })
          .catch((err) => {
            console.log(err)
          })
        }catch(err) {
          console.log(err)
        }
      }
    },
    {
      text: "Detach from iframe",
      execute: () => auth.detachPortal()
    },
    {
      text: "Register state change event",
      execute: () =>
        auth.onStateChange(event => {
          this.setState({
            iframeEvent: this.state.iframeEvent.concat(event)
          });
        })
    }
  ];

  state = {
    host: "http://localhost:9001",
    url: "/#/",
    context: {},
    iframeEvent: [],
    frameSize: {
      h: 667,
      w: 375
    }
  };

  componentDidMount() {

    auth.restore()
    .then(() => {
      this.setState({
        storedCredentials: auth.getCredentialContext()
      });
    });
    client.onPush("push", data => {
      this.setState({ [data.uri]: data.body });
    });
  }

  renderTab = () => {
    return <Tab tabs={[{
      name: 'Key Features (Member)',
      content: <FunctionList
          onError={error => this.setState({ error })}
          functions={this.keyFeatures}
        />
    },{
      name: 'Functions',
      content: <FunctionList
          onError={error => this.setState({ error })}
          functions={this.functions}
        />
    }]}/>
  }

  render() {

    return (
      <div className="app shadow">
      {this.renderTab()}
      {this.renderInfo()}
      </div>
    );
  }

  toggleMobile = () => {
    this.setState({
      frameSize: {
        w: 375,
        h: 667
      }
    });
  };

  toggleDesktop = () => {
    this.setState({
      frameSize: {
        w: 1280,
        h: 960
      }
    });
  };

  renderInfo = () => {
    return <div className="info">
      <ContextView data={this.state} />
    </div>
  }

  urlChanged = e => {
    this.setState({ url: e.target.value });
  };

  goUrl = () => {
    this.frame.src = this.state.host + this.state.url;
  };
}

export default App;

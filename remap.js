module.exports = class Remap {

  apply(compiler) {
    compiler.hooks.emit.tap('RemapPlugin', (compilation) => {

      console.log('[Remap] remapping chunks ..');
      // dummy entry file
      compilation.assets['index.js'] = {
        source: function() {
          return ''
        },
        size: function() {
          return 0
        }
      }

      return true
    });
  }

}
const path = require('path')
const chalk = require('chalk')
const bundleAnalyzer = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const TerserPlugin = require('terser-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const fs = require('fs')
const outputProject = process.env.external_output
const RemapPlugin = require('./remap')
const webpack = require('webpack')
const chokidar = require('chokidar')
const { execSync } = require('child_process')

console.log(chalk`{white The following utils are automatically detected }`)
const utils = fs.readdirSync(path.resolve(__dirname, './src/utils')).reduce((p, f) => {
  if (!f.startsWith('__') && !f.startsWith('index.')) {
    const utilName = f.replace('.ts', '')
    console.log(chalk`[{green.bold ${utilName}}] {gray EntryPoint} {yellow.bold ./src/utils/${f} }`)
    p[utilName] = `./src/utils/${f}`
  }
  return p
}, {})
const externalProjectPath = outputProject ? outputProject.split(',') : []
const defaultOutputPath = path.resolve(__dirname, 'build')
const timeStamp = Date.now()
const mode = process.argv[process.argv.indexOf('--mode')+1]
let syncTimer = null

// watch output folder's change and commit changes to external project paths if there's any
if (externalProjectPath.length > 0) {
  chokidar.watch(path.join(defaultOutputPath))
  .on('change', () => {
    // here's a debounce timer as we don't want unnecessary CPU resource spending
    if (syncTimer) {
      clearTimeout(syncTimer);
    }
    syncTimer = setTimeout(() => {
      externalProjectPath.forEach(p => {
        execSync(`rm -rf ${p}/sdk`)
        execSync(`cp -R ${defaultOutputPath} ${p}/sdk`)
        execSync(`cp -R ${path.resolve(__dirname, './src/defines')} ${p}/sdk/defines`)
        console.log(chalk`{green.bold [sync] } successfully sync output files \n{gray ${defaultOutputPath} } {cyan => }${p}`)
      })
      syncTimer = null
    }, 200)
  })
}

module.exports = {
  mode: 'development',
  entry: {
    ...utils,
    'old-driver-preset': './src/presets/old-driver/index.ts',
    'preset-init': './src/presets/preset-init.ts',
    'web-preset': './src/presets/web/index.ts',
    'web-axios-preset': './src/presets/web-axios/index.ts',
    'web-xhr-preset': './src/presets/web-xhr/index.ts',
    'standalone-lib': './src/presets/standalone/index.ts',
    'ota-preset': './src/presets/ota/index.ts',
    'react-native-preset': './src/presets/react-native/index.ts',
    Auth: './src/packages/Auth/index.ts',
    Network: './src/libraries/Network/index.ts',
    Storage: './src/libraries/Storage/index.ts',
    Constants: './src/constants/index.ts',
    IMC: './src/packages/IMC/index.ts',
    OTAC: './src/packages/OTAC/index.ts',
  },
  externals: {
    axios: 'axios',
  },
  module: {
    rules: [{
      test: /\.ts$/,
      use: 'ts-loader',
      exclude: /node_modules/
    }]
  },
  devtool: 'source-map',
  optimization: {
    minimizer: mode === 'production' ? [
      new TerserPlugin({
        cache: true,
        parallel: true,
        sourceMap: false,
        terserOptions: {
          extractComments: false,
          compress: {
            drop_console: true,
          },
          output: {
            beautify: false
          },
        }
      })
    ] : [],
  },

  resolve: {
    plugins: [
      new TsconfigPathsPlugin({
        configFile: './tsconfig.json',
        baseUrl: path.resolve(__dirname)
      })
    ],
    alias: {
      "@constants": path.resolve(__dirname, "./src/constants"),
      "@mocks": path.resolve(__dirname, "./src/mocks"),
      "@utils": path.resolve(__dirname, "./src/utils"),
      "@defines": path.resolve(__dirname, "./src/defines"),
      "@libraries": path.resolve(__dirname, "./src/libraries"),
      "@packages": path.resolve(__dirname, "./src/packages"),
    },
    extensions: ['.ts', '.js'],
  },
  output: {
    filename: '[name].js',
    // library: ['anue-fe-sdk', '[name]'],
    path: defaultOutputPath,
    jsonpFunction: `anue_sdk_jsonp_${timeStamp}`,
    libraryTarget: "umd",
    umdNamedDefine: true,
    globalObject: '(typeof window === \'undefined\' ? global : window)',
  },
  plugins: [
    new RemapPlugin(),
    new webpack.DefinePlugin({
      DEBUG: JSON.stringify(mode !== 'production')
    })
  ]
  .concat(process.env.ANALYZE ? [
    new bundleAnalyzer()
  ]: [])

}
type ProviderResponse = Anue.Provider.NullableResponse;
type ChangeHandler = Anue.Provider.ChangeHandler;
type EventyHandler<T = any> = (e: T) => void;
type EventyRemover = () => void;
type Constructor<T> = new (...args: any[]) => T;

declare function mockAs<T>(target: any): T;

declare const DEBUG: boolean;

declare const anue: {
  shared: Anue.Shared.SharedContext;
  urld?: Record<string, any>;
};

declare module 'anue-fe-sdk/obfuscator' {
  export const obfuscate = (input: string) => string;
  export const defuscate = (input: string) => string;
}

declare module 'anue-fe-sdk/getty' {
  function getty<T>(obj: any, path: Array<string | number>): T;
  export = getty;
}

declare module 'anue-fe-sdk/eventy' {
  class Eventy {
    handlers = {};
    clear(): void;
    emit(eventName: string | number, data?: any): void;
    on(eventName: string | number, fn: EventyHandler): EventyRemover;
  }
  export = Eventy;
}

declare module 'anue-fe-sdk/Constants' {
  export enum SupportedProviderType {
    Email = 'email',
    Social = 'social'
  }

  export enum SupportedChannel {
    Stock = 'stock',
    MobileApp = 'mobile-app',
    Crypto = 'crypto',
    Driver = 'driver',
    Unknown = 'unknown'
  }
}

declare module 'anue-fe-sdk/APIs' {
  namespace Email {
    export function verifyActivationCode(checkCode: string): Promise<boolean>;
    export function refresh(
      email: string,
      password: string
    ): Promise<Anue.Auth.RefreshTokenResponse>;
    export function register(
      name: string,
      email: string,
      password: string,
      channel: string
    ): Promise<number>;
    export function resend(email: string): Promise<number>;
  }

  namespace Profile {
    export function updatePassword(
      oldPassword: string,
      newPassword: string
    ): Promise<number>;
    export function recoverPassword(email: string): Promise<number>;
    export function resetPassword(
      recoverKey: string,
      password: string
    ): Promise<number>;
    export function updateAvatar(avatar: FormData): Promise<number>;
  }
}

declare module 'anue-fe-sdk/web-bridge-server' {
  interface ServerConfig {
    target: Window;
  }

  class BridgeServer {
    constructor(config: ServerConfig);
    start(): Promise<boolean>;
    use(uri: string, responder: any);
    push(uri: string, payload: any);
  }
  export = BridgeServer;
}

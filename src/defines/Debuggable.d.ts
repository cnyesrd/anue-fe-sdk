namespace Anue {
  namespace Debuggable {
    type Config = {
      type: string | null | undefined;
      name: string | null | undefined;
      prop: string | null | undefined;
    } | null;
    type Logger = (...args) => void;
  }
}

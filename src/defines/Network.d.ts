namespace Anue {
  namespace Network {
    type ResponseInterceptor = (
      requestOptions: Network.RequestOptions,
      response: any,
      extra?: any
    ) => Promise<any>;

    type RequestInterceptor = (
      requestOptions: Network.RequestOptions
    ) => Promise<Network.RequestOptions>;

    interface RequestOptions<T = any> {
      method?: Network.RequestMethod;
      url: string;
      auth?: boolean;
      headers?: Record<string, string> | undefined;
      /**
       * @doc
       * @property body
       * 網路請求的內容，依據實作不同可以有不同的類型
       */
      body?: any;
      /**
       * @doc
       * @property extra
       * 額外的參數可以放在這裡，主要是提供給`intercepter`使用
       */
      extra?: any;
      /**
       * handles必須是一個包含一個至多個額外的ResponseInterceptor的陣列，
       * 和interceptor不同的是，這些函數只會套用到單一的請求之中，並且不影響請求的結果。
       * @doc
       * @property handles?
       */
      handles?: Array<(req: Network.RequestOptions<T>, res: T) => any>;
    }

    interface Response<T = any> {
      statusCode: number;
      headers: Record<string, string> | undefined | Headers;
      body: T | null;
      error: any;
    }

    interface DriverConstructor
      extends Constructor<Anue.Network.AbstractNetworkInterface> {
      /**
       * @doc
       * @method addRequestInterceptor
       * @param fn:Function 用於處理回應請求的[fn:expand_fragment](NetworkInterceptor:network-interceptor)函數
       * @returns Function 呼叫這個函數來移除加入的Interceptor
       * @static
       * @public
       * 加入一個__Interceptor__於當Network的請求處理佇列中，這個方法會返回一個可以將這個__Interceptor__移除的函數
       */
      public static addRequestInterceptor: (
        fn: Anue.Network.RequestInterceptor
      ) => () => void;

      public static clearInterceptors: () => void;

      /**
       * @doc
       * @method addResponseInterceptor
       * @param fn:Function 用於處理回應請求的[fn:expand_fragment](NetworkResponseInterceptor:network-response-interceptor)函數
       * @returns Function 呼叫這個函數來移除加入的Interceptor
       * @static
       * @public
       * 加入一個__Interceptor__於當Network的回應處理佇列中，這個方法會返回一個可以將這個__Interceptor__移除的函數
       */
      public static addResponseInterceptor: (
        fn: Anue.Network.ResponseInterceptor
      ) => () => void;
    }

    class AbstractNetworkInterface {
      constructor();

      protected request<T = any>(
        options: Anue.Network.RequestOptions,
        extraRef: any
      ): Promise<T>;

      public send: <T = any>(
        options: Anue.Network.RequestOptions<T>
      ) => Promise<T>;
    }

    interface PackageDefine {
      getDriver: () => NetworkDriverImpl;
      setDriver: (driver: NetworkDriverImpl) => void;
      AbstractNetworkInterface: AbstractNetworkInterface;
      static clearInterceptors: () => void;
      static addResponseInterceptor: (
        fn: Anue.Network.ResponseInterceptor
      ) => () => void;
      static addRequestInterceptor: (
        fn: Anue.Network.RequestInterceptor
      ) => () => void;
    }
  }
}

declare module 'anue-fe-sdk/Network' {
  export const clearInterceptors: () => void;
  export const addResponseInterceptor: (
    fn: Anue.Network.ResponseInterceptor
  ) => () => void;
  export const addRequestInterceptor: (
    fn: Anue.Network.RequestInterceptor
  ) => () => void;
  export const getDriver: () => Anue.Network.AbstractNetworkInterface;
  export const setDriver: (
    driver: Anue.Network.AbstractNetworkInterface
  ) => void;
  export const AbstractNetworkInterface: Anue.Network.AbstractNetworkInterface;
}

namespace Anue.Shared {

  declare interface SharedContext {
    set(k: string, v: any): void;
    createStatic<T>(key, val): SharedStatic<T>;
    get<T>(name: string): T;
    get(name: 'constants.endpoints'): any;
    get(name: 'constants.keys'): Anue.Constants;
    get(name: 'utils.sso'): Anue.Utils.SSO;
    get(name: 'utils.getty'): Anue.Utils.Getty;
    get(name: 'utils.eventy'): Anue.Utils.Eventy;
    get(name: 'utils.urld'): Anue.Utils.Urld;
    get(name: 'utils.urly'): Anue.Utils.Urly;
    get(name: 'utils.isomorphy'): Anue.Utils.Isomorphy;
    get(name: 'utils.rpc'): Anue.Utils.RPC;
    get(name: 'utils.res'): Anue.Utils.Res;
    get(name: 'utils.waitty'): Anue.Utils.Waitty;
    get(name: 'utils.paramly'): Anue.Utils.Paramly;
    get(name: 'utils.obfuscator'): Anue.Utils.Obfuscator;
    get(name: 'library.net.abstract'): Anue.Network.DriverConstructor;
    get(name: 'library.net'): Anue.Network.PackageDefine;
    get(name: 'packages.imc'): typeof import ('anue-fe-sdk/IMC');
    get(name: 'packages.auth'): AnueAuth;
    get(name: 'packages.otac'): Anue.OTAC.OTACManager;
    get(name: 'extern.trackjs'): Anue.Tracking.Compat;
  }

  declare interface SharedStatic<T> {
    get: () => T;
    set: (v: T) => void;
    createStatic<T>(key: string, val: T): SharedStatic<T>;
  }
}

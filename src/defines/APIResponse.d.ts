declare namespace APIResponse {
  export interface Response<T> {
    statusCode: number;
    message: string;
    items: T;
  }

  interface ServiceResponse<T = any> {
    status_code: number;
    message: string;
    items: T
  }

  export interface PagedData<T> {
    total: number;
    per_page: number;
    current_page: number;
    last_page: number;
    next_page_url: string | null;
    prev_page_url: string | null;
    from: number;
    to: number;
    data: T[];
  }

  namespace v2 {
    namespace User {
      interface Profile {
        memberId: number;
        uid: string;
        name: string;
        email: string;
        nickname: string;
        gender: string;
        avatar: string;
        isVip: 0 | 1;
        onBlackList: 0 | 1;
      }
    }
  }

  namespace v1 {
    namespace User {
      interface Token {
        token: string;
      }

      namespace Profile {
        interface Password {
          message: string;
          statusCode: number;
        }

        namespace Password {
          interface Recovery {
            items: boolean;
            message: string;
            statusCode: number;
          }

          interface Reset {
            message: string;
            statusCode: number;
          }
        }

        interface Avatar {
          items: boolean;
          message: string;
          statusCode: number;
        }
      }

      namespace Email {
        interface Renew {
          identityId: string;
          token: string;
        }

        interface Resend {
          items: string;
          message: string;
          statusCode: number;
        }
      }

      namespace Registration {
        interface Email {
          message: string;
          statusCode: number;
        }

        namespace Email {
          interface Verify {
            verify: boolean;
          }
        }
      }

      interface CognitoOpenIdOrRegister {
        identityId: string;
        token: string;
        newcomer: 0 | 1;
      }

      interface TOS {
        message: string;
        statusCode: number;
      }

      namespace TOS {
        interface All {
          items: string[];
          message: string;
          statusCode: number;
        }

        interface User {
          items: string;
          message: string;
          statusCode: number;
        }
      }
    }
  }
}

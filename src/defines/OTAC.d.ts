// import * as React from 'react'
// import * as ReactDOM from 'react-dom'

namespace Anue.OTAC {

  interface OTACManager {
    setServer(url: string): OTACManager;
    addRenderEngine(engine: RenderEngineDef): OTACManager;
    loadComponent(id: string, version: string, replace?: boolean): Promise<void>;
    finish(id: string, version: string, module?: any): void;
  }

  interface OTACContext {
    server: string;
    loaded: Record<string, string>;
    engines: Record<string, RenderEngineDef>;
    service?: OTACManager;
    requests: Record<string, Promise>;
  }
  interface RenderEngineDef {
    ReactCompat?: {
      React: React;
      ReactDOM: React.ReactDOM;
    }
    code: string;
  }
}

declare module 'anue-fe-sdk/OTAC' {
  const service: Anue.OTAC.OTACManager
  export default service
}

namespace Anue {
  interface SupportedProviderType {
    Social: 'social';
    Email: 'email';
  }

  interface SupportedChannel {
    Stock: 'stock';
    MobileApp: 'mobile-app';
    Crypto: 'crypto';
    Driver: 'driver';
    Unknown: 'unknown';
  }
  interface Constants {
    Auth: {
      Events: {
        StateChange: 1;
        Update: 2;
        Error: 3;
        Loaded: 4;
      };
      Storage: {
        CognitoId: 'anue.cognitoId';
        IdToken: 'anue.idToken';
        AccessToken: 'anue.accessToken';
        Authorization: 'anue.authorization';
        NewComer: 'anue.newcomer';
        Provider: 'anue.provider';
        Profile: 'anue.profile';
        SSOProvider: 'anue.sso';
      };
    };
    Debuggable: {
      Config: 'anue.debuggable.config';
      Logger: 'anue.debuggable.logger';
    };
    Storage: {
      Driver: 'anue.storage.driver';
      Abstract: 'anue.storage.abstract';
    };
    Network: {
      Driver: 'anue.network.driver';
      Abstract: 'anue.network.abstract';
    };
    SupportedChannel;
    SupportedProviderType;
  }

  interface ValidEndpoints {
    v2: {
      user: {
        cognitoOpenIdOrRegister: () => string;
        new: {
          profile: () => string;
        };
        profile: () => string;
      };
    };
    v1: {
      user: {
        notifications: () => string;
        notification: {
          (): string;
          redirect: () => string;
          read: () => string;
          unread: () => string;
        };
        profile: {
          (): string;
          password: {
            (): string;
            recovery: () => string;
            reset: () => string;
          };
          avatar: () => string;
        };
        email: {
          renew: () => string;
          resend: () => string;
        };
        cognitoOpenIdOrRegister: () => string;
        tos: {
          (): string;
          all: () => string;
          user: () => string;
        };
        registration: {
          email: {
            (): string;
            verify: () => string;
          };
        };
      };
    };
  }
}

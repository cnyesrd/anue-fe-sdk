namespace Anue {
  namespace DPI {
    interface Payload {
      type: string;
      error?: any;
      data: any;
    }
  }
}

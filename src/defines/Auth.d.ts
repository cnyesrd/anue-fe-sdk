namespace Anue.Auth {
  type SupportedProviderType = 'social' | 'email';
  type SupportedChannel =
    | 'stock'
    | 'mobile-app'
    | 'crypto'
    | 'driver'
    | 'unknown';

  type RefreshTokenResponse = APIResponse.Response<{
    identityId: string;
    token: string;
  }>;

  interface ExistingState {
    flag?: string;
    token: string;
  }

  type AuthState = 'online' | 'offline' | 'connecting';

  type CachedUser = {
    cognitoId: string;
    profile: Anue.Auth.UserProfile;
    time: number;
  } | null;

  interface UserProfile {
    avatar: string;
    name: string;
    email: string;
    uid: string;
    gender?: string;
    birth?: string;
    // legacy property used in member 1.0
    birthYear?: string;
    // format YYYY-MM-DD
    // legacy property used in member 1.0
    birthDate?: string;
    phone?: string;
    postal_code?: string;
    // legacy property used in member 1.0
    phoneNumber?: string;
    // legacy property used in member 1.0
    postalCode?: string;
    country?: string;
    address?: string;
  }

  interface Config {
    formatToken?: (token: string) => string;
    type?: SupportedProviderType;
    channel?: SupportedChannel;
    variation?: string;
    host?: string;
    imc?: boolean;
    isOpener?: boolean;
    singleProvider?: boolean;
    rpcHost?: string;
    alwaysSendToken?: boolean;
  }

  interface StateChangeEvent {
    status: 'online' | 'offline';
    flag: string;
    context?: Credentials;
    source: string;
  }

  interface ErrorChangeEvent {
    detail: any;
    message?: string;
    source: string;
  }

  interface Credentials {
    authorization: string | undefined;
    flag?: string;
    profile: UserProfile | null;
  }

  interface AuthContext {
    host: string;
    channel: SupportedChannel;
  }
}

declare class AuthService {
  constructor(config: Anue.Auth.Config = {});
  event: Eventy;
  host(memberServiceHostUrl: string): AuthService;
  refreshToken(): Promise<RefreshTokenResponse>;
  renew(): Promise<Anue.Auth.Credentials | null>;
  consume(): Promise<boolean>;
  consumeExisting(data: Anue.Auth.ExistingState): Auth;
  getCredentialContext(): Anue.Auth.Credentials | null;
  onStateChange(fn: EventyHandler<Anue.Auth.StateChangeEvent>): Auth;
  onError(fn: EventyHandler<Anue.Auth.ErrorChangeEvent>): Auth;
  clearCredentials(): void;
  updateEmail(email: string): Promise<{ error: any }>;
  isLogin(): boolean;
  optimisticallyGetProfile(): Anue.Auth.UserProfile;
}

type AnueAuth = new (config: Anue.Auth.Config = {}) => AuthService;

declare module 'anue-fe-sdk/Auth' {
  import Constants from 'anue-fe-sdk/Constants';
  import AbstractProvider from 'anue-fe-sdk/AbstractProvider';
  import Eventy from 'anue-fe-sdk/eventy';
  export = AuthService;
}

namespace Anue.Utils {
  interface SSO {
    updateSSOCookie: (data: any) => void;
    setSSOCookie: (token: string) => void;
    getSSOCookie: () => string | null;
    getCachedUser: () => {
      cognitoId: string;
      profile: Anue.Auth.UserProfile;
      time: number;
    } | null;
    cacheValidUser: (
      cognitoId: string,
      profile: Anue.Auth.UserProfile,
      ttl?: number
    ) => void;
    getCachedContext: () => { token: string; provider: string } | null;
    removeCachedContext: () => void;
    cacheContext: (
      ctx: { token: string; provider: string },
      ttl?: number
    ) => void;
    setAuthFlag: (flag: string) => void;
    getAuthFlag: () => string | null;
    removeCachedUser: () => void;
    removeSSOCookie: () => void;
    setEmailUser: (email: string, password: string) => void;
    getEmailUser: () => [string, string] | null;
  }

  type Waitty = (time: number) => Promise<void>;

  type Getty<T = any> = (
    target: any,
    path: Array<number | string>
  ) => T | undefined;

  type EventyHandler<T = any> = (data: T) => void;
  type EventyRemover = () => void;

  interface Res {
    loadScript: (url: string, id?: string, replace?: boolean) => Promise<void>;
    loadCSS: (url: string) => Promise<void>;
  }

  interface Eventy {
    new ();
    clear: () => void;
    emit: (eventName: string | number, data?: any) => void;
    on: <T = any>(
      eventName: string | number,
      fn: EventyHandler<T>
    ) => EventyRemover;
  }
  type Urly = (...args: string[]) => string;

  interface Urld {
    init: () => void;
    parse: () => Record<string, string>;
    get: <T = any>(key) => T;
  }
  interface RPC {
    new (tag: string, proxyUri: string);
    rpcCall(method: string, args: Array<string | number>): Promise<any>;
  }
  interface Obfuscator {
    obfuscate(input: string, key = 20): string;
    defuscate(input: string): string;
  }
  interface Paramly {
    (input: Record<string, string | number>): string;
    toObject: (url: string) => Record<string, string>;
  }
}

declare module 'anue-fe-sdk/sso' {
  const sso: Anue.Utils.SSO;
  export = sso;
}

declare module 'anue-fe-sdk/getty' {
  const getty: Anue.Utils.Getty;
  export = getty;
}

declare module 'anue-fe-sdk/eventy' {
  const Eventy: Anue.Utils.Eventy;
  export = Eventy;
}

declare module 'anue-fe-sdk/rpc' {
  const RPC: Anue.Utils.RPC;
  export = RPC;
}

declare module 'anue-fe-sdk/waitty' {
  const Waitty: Anue.Utils.Waitty;
  export = Waitty;
}

declare module 'anue-fe-sdk/paramly' {
  const Paramly: Anue.Utils.Paramly;
  export = Paramly;
}

declare module 'anue-fe-sdk/urly' {
  const Urly: Anue.Utils.Urly;
  export = Urly;
}

declare module 'anue-fe-sdk/res' {
  const Res: Anue.Utils.Res;
  export = Res;
}

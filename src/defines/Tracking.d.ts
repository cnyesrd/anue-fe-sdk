namespace Anue.Tracking {
  // compatible tracking object
  interface Compat {
    error: (...args: any[]) => void;
    warn: (...args: any[]) => void;
    info: (...args: any[]) => void;
  }
}

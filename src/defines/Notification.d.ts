declare module 'anue-fe-sdk/Notification' {
  class NotificationManager {
    /**
     * Set update message status to `read` by given ids.
     * @param ids An array contains message ids.
     * @see http://api.beta.cnyes.cool/api-docs/cnyes-api#!/%5BPublic%5D_Member.notification/redirectUrl_0_1
     */
    getMessages(
      params: Anue.Notification.GetMessagesParams = {}
    ): Promise<APIResponse.PagedData<Anue.Notification.Message>>;

    /**
     * Set update message status to `read` by given ids.
     * @see http://api.beta.cnyes.cool/api-docs/cnyes-api#!/%5BPublic%5D_Member.notification/redirectUrl
     * @param ids An array contains message ids.
     */
    markMessagesAsRead(params: Anue.Notification.SendReadParams): Promise<void>;
  }
  export = NotificationManager;
}

namespace Anue {
  namespace Notification {
    interface Message {
      id: number;
      message: string;
      link: string;
      startAt: number;
      endAt: number | null;
      iconUrl: string;
      createdAt: number;
      updatedAt: number;
      isRead: boolean;
    }
    interface GetMessagesParams {
      // 通知訊息狀態{0:未讀|1:已讀|2:已讀+未讀}, 預設為1
      status?: 0 | 1 | 2;
      // 是否只取有效區間內的通知{0:false|1:true}, 預設為1
      active?: 0 | 1;
      // 排序方向{asc:升冪|desc:降冪}, 預設為 desc
      sortMethod?: 'asc' | 'desc';
      // 要排序的欄位, 用逗號分隔{startAt,endAt,messageType}
      sortFields?: string;
      // 頁數, 預設為1
      page?: number;
      // 一頁幾則, 預設為20
      limit?: number;
    }

    interface SendReadParams {
      ids: number[];
    }
  }
}

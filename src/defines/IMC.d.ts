namespace Anue.IMC {
  type IMCMethod<T, R> = (a: T) => Promise<Anue.IMC.Response<R>>;

  namespace DatePicker {
    type openWithXYResponse = null | {
      year: number;
      month: number;
      day: number;
    };
  }

  interface MethodDefine {
    name: string;
    handle: (a: any) => any;
  }

  interface Request {
    name: string;
    handle: (args: any) => void | Response<any>;
  }

  type Response<T> = Promise<{
    data: T;
  }>;
}

declare module 'anue-fe-sdk/IMC' {
  export function has(module: string): boolean;
  export function register(
    method: string,
    handle: (...args: any[]) => any
  ): void;
  export function call(
    /**
     * 打開Date Picker並且回傳一個 `Promise`。
     * 當這使用者選取一個日期並按下 `確認` 時這個 `Promise` 即返回所選擇的日期。
     * 必要參數: `args: { x: number, y: number }`
     */
    method: 'dp.openWithXY',
    /**
     * 開啟ota date picker的初始化位置參數。
     */
    args: { x: number; y: number }
  ): Anue.IMC.Response<Anue.IMC.DatePicker.openWithXYResponse>;
  export function call(method: string, ...args: any[]): any;
  export function call(
    /**
     * 透過IMC監聽`anue-fe-sdk`的`Auth`所產生的登入狀態變化事件。
     */
    method: 'auth.stateChange',
    /**
     * 當`Auth`產生登入狀態變化時將會觸發這個函數，在第一個參數中為登入狀態的資訊。
     */
    args: EventyHandler<Anue.Auth.StateChangeEvent>
  ): Anue.IMC.Response<EventyRemover>;
  export function call(
    /**
     * 這個IMC方法等同於`Auth.consumeExisting`，用來將現有的token進行儲存並變更使用者的登入狀態
     */
    method: 'auth.consume',
    /**
     * 當`Auth`產生登入狀態變化時將會觸發這個函數，在第一個參數中為登入狀態的資訊。
     */
    args: { token: string; flag: string }
  ): Anue.IMC.Response<void>;
  export function call(
    /**
     * 此方法在`anue-fe-sdk/Auth`實例產生時會自動註冊，如果要避免重複註冊該IMC方法，可以在建構子中
     * 加入參數 `imc: false` 來避免重複註冊。
     */
    method: 'auth.getCtx'
  ): Anue.IMC.Response<Anue.Auth.Credentials>;
  export function call(
    /**
     * 該方法在`auth`模組載入後被註冊，主要是切換登入窗的顯示狀態。
     * 值得注意的是，當登入窗被隱藏後下次開啟時預設會顯示`登入頁面`。
     */
    method: 'ota-auth.toggle',
    /**
     * 設定OTA Auth元件是否為可見的。
     */
    visible: boolean,
    /**
     * 此為可選參數，用來調整元件登入頁面上方的文字敘述，若不給值則使用預設文字敘述。
     */
    wording?: string
  ): Anue.IMC.Response<void>;
  export function call(
    /**
     * 隱藏Date Picker，這個方法會觸發上一個 `dp.openWithXY` 的Promise使其返回 `{ data: null }`
     */
    method: 'dp.hide'
  ): Anue.IMC.Response<void>;
  export function call(
    /**
     * 這個方法是會在`client-notification`模組載入後被註冊，主要是用來觸發該模組取得最新的站內通知訊息。
     */
    method: 'notification.fetch'
  ): Anue.IMC.Response<void>;
}

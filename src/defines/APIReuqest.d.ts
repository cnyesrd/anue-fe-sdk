namespace APIRequest {
  namespace v2 {
    namespace User {
      interface ProfileHeader {
        Authorization: string;
        'X-CNYES-APP': string;
      }
    }
  }
  namespace v1 {
    namespace User {
      namespace Profile {
        interface Password {
          oldPassword: string;
          newPassword: string;
        }

        namespace Password {
          interface Recovery {
            email: string;
          }

          interface Reset {
            recoverKey: string;
            password: string;
          }
        }

        interface Avatar {
          avatar: FormData;
        }
      }

      namespace Email {
        interface Renew {
          email: string;
          password: string;
        }

        interface Resend {
          email: string;
        }
      }

      interface CognitoOpenIdOrRegister {
        type: Network.ProviderType;
        idToken?: string;
        accessToken: string;
        refresh?: string;
        channel?: Network.Channel;
      }

      interface TOS {
        platform: string; // TODO 確認平台與接受的服務名稱
        services: 'ALL' | 'OLD_DRIVER';
      }
    }
  }
}

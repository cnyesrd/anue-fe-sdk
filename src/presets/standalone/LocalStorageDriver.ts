import StorageAbstract from '@libraries/Storage/Storage.abstract';

if (typeof localStorage === 'undefined') {
  console.warn(
    `LocalStorageDriver is not supported in this environment, try use other drivers.`
  );
}

class LocalStorageDriver extends StorageAbstract {
  removeItemImpl = async (key): Promise<boolean> => {
    delete localStorage[key];
    return await true;
  };

  setItemImpl = async (key: string, value: any): Promise<boolean> => {
    localStorage[key] = value;
    return await true;
  };

  getItemImpl = async (key: string): Promise<string> => {
    return await localStorage[key];
  };
}

class LocalStorageNotSupportedDriver extends StorageAbstract {
  protected setItemImpl = async (
    key: string | number,
    value: any
  ): Promise<boolean> => {
    return await true;
  };
  protected getItemImpl = async (key: string | number): Promise<string> => {
    return await '';
  };
  protected removeItemImpl = async (key: string | number): Promise<boolean> => {
    return await true;
  };
}

export default (typeof localStorage === 'undefined'
  ? LocalStorageNotSupportedDriver
  : LocalStorageDriver);

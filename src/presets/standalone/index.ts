import Shared from '@utils/shared';
import '@constants/Keys';
import '@constants/Endpoints';
import '@utils/waitty';
import '@utils/getty';
import '@utils/eventy';
import '@utils/rpc';
import '@utils/obfuscator';
import '@utils/paramly';
import '@utils/sso';
import '@utils/res';
import '@utils/urly';
import urld from '@utils/urld';
import '@libraries/Network/Network.abstract';
import * as Network from '@libraries/Network';
import * as Storage from '@libraries/Storage';
import NetworkDriver from '../XHRNetworkDriver';
import LocalStorageDriver from './LocalStorageDriver';

import '@packages/OTAC';
import '@packages/IMC';
import '@packages/Auth';

Shared.set('preset-init', () => {
  urld.init();
});

Storage.setDriver(new LocalStorageDriver());
Network.setDriver(new NetworkDriver());

// @ts-ignore
anue.standalone = {
  ai: null,
  async init(onChange) {
    const Auth = anue.shared.get('packages.auth');
    const auth = new Auth();
    // @ts-ignore
    anue.standalone.ai = auth;
    if (onChange) {
      auth.onStateChange(onChange);
    }
    auth.host('https://member.prod.cnyes.com/member')
    return await auth.consume();
  },
  login() {

  },
  async loginWithUI() {
    const OTAC = anue.shared.get('packages.otac');
    const IMC = anue.shared.get('packages.imc');

    OTAC.setServer('https://login.cnyes.com');
    await OTAC.loadComponent('auth', 'latest');
    IMC.call('ota-auth.toggle', true);
  }
};

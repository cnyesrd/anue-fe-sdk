type Constructor<T> = new (...args: any[]) => T;

const BaseNetworkDriver: Constructor<
  Anue.Network.AbstractNetworkInterface
> = anue.shared.get('library.net.abstract');

class XHRNetworkDriver extends BaseNetworkDriver {
  /**
   * Send a request and resolves the response, this only throws error
   * when things going wrong with network connection.
   * otherwise it resolves an Anue.Response object with status code,
   * body and other information.
   * @param options Request options like url, method, headers, and body
   */
  request = <T = any>(options: Anue.Network.RequestOptions): Promise<T> => {
    if (options && typeof options.body === 'object') {
      /**
       * @TODO Research default `Content-Type` behavior of Fetch API
       */
      options.headers = options.headers || {};
      if (options.body instanceof FormData === false) {
        options.headers['content-type'] = 'application/json';
        options.body = JSON.stringify(options.body);
      }
    }

    const xhr = new XMLHttpRequest();

    xhr.open(options.method, options.url);

    for (const h in options.headers) {
      xhr.setRequestHeader(h, options.headers[h]);
    }

    return new Promise((resolve, reject) => {
      xhr.onreadystatechange = () => {
        if (xhr.readyState === XMLHttpRequest.DONE) {
          if (xhr.status > 299) {
            reject(xhr.response);
          } else {
            switch (xhr.responseType) {
              case 'json':
                resolve(JSON.parse(xhr.response));
                break;
              default:
                try {
                  resolve(JSON.parse(xhr.response));
                } catch (err) {
                  resolve(xhr.response);
                }
                break;
            }
          }
        }
      };
      xhr.onerror = reject;
      xhr.send(options.body);
    });
  };
}

class XHRNotSupportedDriver extends BaseNetworkDriver {
  protected request = async <T = any>(
    options: Anue.Network.RequestOptions
  ): Promise<T> => {
    return (await {}) as T;
  };
}

export default (typeof XMLHttpRequest === 'undefined'
  ? XHRNotSupportedDriver
  : XHRNetworkDriver);

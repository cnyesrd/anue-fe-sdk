import '@utils/getty';
import '@utils/sso';
import '@utils/eventy';
import '@utils/rpc';
import * as Storage from '@libraries/Storage';
import * as Network from '@libraries/Network';
import AxiosNetworkDriver from '../AxiosNetworkDriver';
import LocalStorageDriver from './LocalStorageDriver';

Storage.setDriver(new LocalStorageDriver());
Network.setDriver(new AxiosNetworkDriver());

export { Storage, Network };

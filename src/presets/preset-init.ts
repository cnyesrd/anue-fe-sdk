const fn = anue.shared.get<() => void>('preset-init');

if (typeof fn === 'function') {
  fn();
}

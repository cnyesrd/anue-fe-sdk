import axios, { AxiosResponse } from 'axios';

const BaseNetworkDriver = anue.shared.get('library.net.abstract');

class AxiosDriverNetworkDriver extends BaseNetworkDriver {
  private instance = axios.create();

  /**
   * Send a request and resolves the response, this only throws error
   * when things going wrong with network connection.
   * otherwise it resolves an Anue.Response object with status code,
   * body and other information.
   * @param options Request options like url, method, headers, and body
   */
  request = async <T = any>(
    options: Anue.Network.RequestOptions,
    extra?: any
  ): Promise<T> => {
    let response: any = null;

    if (options && typeof options.body === 'object') {
      options.headers = {
        'content-type': 'application/json',
        ...options.headers
      };
      options.body = JSON.stringify(options.body);
    }

    try {
      const res: AxiosResponse = await this.instance.request({
        url: options.url,
        method: options.method || 'get',
        headers: options.headers,
        data: options.body
      });
      extra.status = res.status;
      response = await res.data;
    } catch (error) {
      if (error) {
        if (error.response) {
          response = error.response.data;
          extra.status = error.response.status;
        }
      } else {
        extra.status = 4999;
      }
    }
    if (!response) {
      response = { nativeStatus: extra.status || 4999 }
    }
    else {
      response.nativeStatus = extra.status || 4999;
    }

    return response;
  };
}

// Provide aliases for supported request methods
['delete', 'get', 'head', 'options'].forEach(function forEachMethodNoData(
  method
) {
  AxiosDriverNetworkDriver.prototype[method] = function(url, config) {
    return this.send({
      ...config,
      method,
      url
    });
  };
});

['post', 'put', 'patch'].forEach(function forEachMethodWithData(method) {
  AxiosDriverNetworkDriver.prototype[method] = function(url, data, config) {
    return this.send({
      ...config,
      method,
      url,
      body: data
    });
  };
});

export default AxiosDriverNetworkDriver;

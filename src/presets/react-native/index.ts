import * as Storage from '@libraries/Storage';
import * as Network from '@libraries/Storage';
import Keys from '@constants/Keys';
import SharedContext from '@utils/shared';
import FetchNetworkDriver from './FetchNetworkDriver';
import AsyncStorageDriver from './AsyncStorageDriver';

SharedContext.set(Keys.Storage.Driver, new AsyncStorageDriver());
SharedContext.set(Keys.Network.Driver, new FetchNetworkDriver());

export { Storage, Network };

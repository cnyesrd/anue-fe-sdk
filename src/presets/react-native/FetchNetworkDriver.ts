import AbstractNetworkInterface from '@libraries/Network/Network.abstract';

export enum ContentTypeEnum {
  JSON,
  BLOB,
  TEXT,
  UNKNOWN
}

export default class FetchNetworkDriver extends AbstractNetworkInterface {
  private static getContentType = (mime: string | null): ContentTypeEnum => {
    if (typeof mime !== 'string') return ContentTypeEnum.UNKNOWN;

    if (mime.includes('application/json')) return ContentTypeEnum.JSON;
    else if (mime.includes('text/')) return ContentTypeEnum.TEXT;
    else if (
      mime.includes('audio') ||
      mime.includes('video') ||
      mime.includes('image') ||
      mime.includes('application/octet')
    )
      return ContentTypeEnum.BLOB;
    return ContentTypeEnum.UNKNOWN;
  };

  /**
   * Send a request and resolves the response, this only throws error
   * when things going wrong with network connection.
   * otherwise it resolves an Anue.Response object with status code,
   * body and other information.
   * @param options Request options like url, method, headers, and body
   */
  request = async <T = any>(
    options: Anue.Network.RequestOptions
  ): Promise<T> => {
    const res: Response = await fetch(options.url, {
      method: options.method,
      headers: options.headers,
      body: options.body
    });

    const mime = res.headers.get('content-type') || null;
    const contentType = FetchNetworkDriver.getContentType(mime);
    let body: any = null;

    switch (contentType) {
      case ContentTypeEnum.BLOB:
        body = await res.blob();
        break;
      case ContentTypeEnum.JSON:
        body = await res.json();
        break;
      default:
        body = await res.text();
        break;
    }

    return await body;
  };
}

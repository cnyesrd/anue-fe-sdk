import Shared from '@utils/shared';
import '@constants/Keys';
import '@constants/Endpoints';
import '@utils/waitty';
import '@utils/getty';
import '@utils/eventy';
import '@utils/rpc';
import '@utils/obfuscator';
import '@utils/sso';
import urld from '@utils/urld';
import '@libraries/Network/Network.abstract';
import * as Network from '@libraries/Network';
import * as Storage from '@libraries/Storage';
import XHRNetworkDriver from '../XHRNetworkDriver';
import LocalStorageDriver from './LocalStorageDriver';

import '@packages/OTAC';
import '@packages/IMC';
import '@packages/Auth';

Shared.set('preset-init', () => {
  urld.init();
});

Storage.setDriver(new LocalStorageDriver());
Network.setDriver(new XHRNetworkDriver());

export { Storage, Network };

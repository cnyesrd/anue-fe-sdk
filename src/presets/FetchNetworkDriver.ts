const BaseNetworkDriver = anue.shared.get('library.net.abstract');

export enum ContentTypeEnum {
  JSON,
  BLOB,
  TEXT,
  UNKNOWN
}

class FetchNetworkDriver extends BaseNetworkDriver {
  private static getContentType = (mime: string | null): ContentTypeEnum => {
    if (typeof mime !== 'string') {
      return ContentTypeEnum.UNKNOWN;
    }
    if (mime.includes('application/json')) {
      return ContentTypeEnum.JSON;
    } else if (mime.includes('text/')) {
      return ContentTypeEnum.TEXT;
    } else if (
      mime.includes('audio') ||
      mime.includes('video') ||
      mime.includes('image') ||
      mime.includes('application/octet')
    ) {
      return ContentTypeEnum.BLOB;
    }
    return ContentTypeEnum.UNKNOWN;
  };

  /**
   * Send a request and resolves the response, this only throws error
   * when things going wrong with network connection.
   * otherwise it resolves an Anue.Response object with status code,
   * body and other information.
   * @param options Request options like url, method, headers, and body
   */
  request = async <T = any>(
    options: Anue.Network.RequestOptions,
    extra?: any
  ): Promise<T> => {
    if (options && typeof options.body === 'object') {
      /**
       * @TODO Research default `Content-Type` behavior of Fetch API
       */
      options.headers = options.headers || {};
      if (options.body instanceof FormData === false) {
        options.headers['content-type'] = 'application/json';
        options.body = JSON.stringify(options.body);
      }
    }

    let body: any = null;
    try {

      const res: Response = await fetch(options.url, {
        method: options.method,
        headers: options.headers,
        body: options.body
      });

      const mime = res.headers.get('content-type') || null;
      const contentType = FetchNetworkDriver.getContentType(mime);

      switch (contentType) {
        case ContentTypeEnum.BLOB:
          body = await res.blob();
          break;
        case ContentTypeEnum.JSON:
          body = await res.json();
          break;
        default:
          body = await res.text();
          break;
      }
      extra.status = res.status;
    }
    catch (err) {
      extra.status = 4999;
      extra.message = err;
    }
    if (body) {
      body.nativeStatus = extra.status;
    }
    else {
      body = {
        nativeStatus: extra.status
      }
    }
    return body;

  };
}

class FetchNotSupportedDriver extends BaseNetworkDriver {
  request = async <T = any>(
    options: Anue.Network.RequestOptions
  ): Promise<T> => {
    return (await {}) as T;
  };
}

export default (typeof fetch === 'undefined'
  ? FetchNotSupportedDriver
  : FetchNetworkDriver);

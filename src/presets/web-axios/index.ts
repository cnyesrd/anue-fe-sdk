import Shared from '@utils/shared';
import '@constants/Keys';
import '@constants/Endpoints';
import '@utils/waitty';
import '@utils/getty';
import '@utils/eventy';
import '@utils/rpc';
import '@utils/obfuscator';
import '@utils/paramly';
import '@utils/sso';
import '@utils/res';
import '@utils/urly';
import urld from '@utils/urld';
import '@libraries/Network/Network.abstract';
import * as Network from '@libraries/Network';
import * as Storage from '@libraries/Storage';
import AxiosNetworkDriver from '../AxiosNetworkDriver';
import LocalStorageDriver from './LocalStorageDriver';

import '@packages/OTAC';
import '@packages/IMC';
import '@packages/Auth';

Shared.set('preset-init', () => {
  urld.init();
});

Storage.setDriver(new LocalStorageDriver());
Network.setDriver(new AxiosNetworkDriver());

export { Storage, Network };

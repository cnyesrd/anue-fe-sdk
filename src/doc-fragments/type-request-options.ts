/**
 * @doc
 * @anue-fragment type-request-options
 * @name RequestOptions
 * @attr method? 請求的method，必須為 `get` | `post` | `put` | `delete`
 * @attr url 請求目標的URL字串
 * @attr headers? 請求的header物件
 * @attr body? 請求的內容，當`method` 為`get`或`delete`時會被忽略
 * @attr extra? 額外的物件，通常會被[Network.interceptor](->Anue.Network#interceptor)所添加或修改
 * 
 * Network在送出一個請求時的設定參數物件，參考以下.d.ts中的定義
 * 
 * ```TypeScript
 * interface RequestOptions {
 *     method?: Network.RequestMethod;
 *     url: string;
 *     headers?: Record<string, string> | undefined;
 *     body?: any;
 *     extra?: any;
 *   }
 * ```
 * 
 */
/**
 * @anue-module Anue
 * @doc
 * 鉅亨網前端sdk的主要命名空間，在使用SDK之前請先注意使用的環境是否符合以下需求:
 *
 * - 支援`Promise`以及`ES6`語法
 * - 所使用的模組支援`CommonJS`等解析方式
 *
 * ### 使用及安裝
 *
 * >! 為了專案開發上的一致，請使用 `yarn` 來進行安裝
 *
 * 進入專案根目錄下透過以下指令進行安裝特定版本
 *
 * ```Shell
 * $ yarn add https://bitbucket.org/benhsieh0404/anue-fe-sdk.git#這邊輸入版本
 * ```
 * 接著便可以在應用中使用SDK進行開發
 *
 * ```JS
 * import 'anue-fe-sdk'
 * import 'anue-fe-sdk/web-preset'
 * import Auth from 'anue-fe-sdk/Auth'
 *
 * const auth = new Auth().config({
 *    host: 'http://api.beta.cnyes.cool',
 *    channel: 'driver'
 * })
 *
 * ```
 *
 * 以上範例中的前兩行是每個應用程式在開始前都必須先執行一次的，以確保實作和當前的環境相容。
 *
 * ### SDK所附帶的Util
 *
 * 一些經常使用到的功能在SDK的`Util`中都已經有簡單並且有效率的實作，像是 [fn:expand_fragment](utils.getty:utils-getty)
 * 就是一個可以取代`idx`和`lodash.get`的方案，因此在應用程式開發時請評估是否再額外安裝其他工具。
 *
 * ### 開發SDK
 *
 * 在開發SDK上建議使用`VSCode`來進行開發，主要基於以下兩點考量：
 * - 本專案使用`TypeScript`為主要的開發語言，因此VSCode的整合是最好的
 * - 專案中也對VSCode設定了許多方便開發的Task，可以提高開發效率
 *
 * 開發上基本上只要透過VSCode中設定好的`Jest Current File`等工作配合撰寫單元測試就可以進行。
 * 然而，如果想要在實際的環境上安裝Webpack打包過後的SDK則需要透過專案中設定好的`sandbox`來運行。
 *
 * TODO
 */

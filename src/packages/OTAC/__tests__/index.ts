declare const mockAs: <T>(target: any) => T;

jest.mock('@utils/res');

import OTAC from '@packages/OTAC';
import * as res from '@utils/res';

const MockRes = mockAs<{
  mockCSS: (url, id, handle) => void;
  mockScript: (url, id, handle) => void;
  resetMock: () => void;
}>(res);

anue.shared.set('utils.res', MockRes);

beforeAll(() => {
  jest.resetModules();
});

describe('OTAC manager tests', () => {
  it('should request correct url after set OTAC server via OTAC.setServer', async done => {
    const server = `mock://${Math.random()}`;
    const spy = jest.fn();
    const url = `${server}/mock.latest.js`;

    OTAC.setServer(server);
    MockRes.mockScript(url, '', spy);
    OTAC.loadComponent('mock', 'latest').then(() => {
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(url);
      done();
    });
    OTAC.finish('mock', 'latest');
  });

  it('loadComponent should resolve the promise at right timing', async done => {
    const server = `mock://${Math.random()}`;
    const url = `${server}/mock.latest.js`;

    OTAC.setServer(server);
    MockRes.mockScript(url, '', async () => {
      return true;
    });
    let results: any[] = [];
    const requests = Promise.all([
      OTAC.loadComponent('mock', 'latest'),
      OTAC.loadComponent('mock', 'latest'),
      OTAC.loadComponent('mock', 'latest'),
      OTAC.loadComponent('mock', 'latest'),
      OTAC.loadComponent('mock', 'latest')
    ]);

    requests.then(actual => {
      results = actual;
      expect(results).toHaveLength(5);
      done();
    });

    setTimeout(() => {
      expect(results).toHaveLength(0);
      OTAC.finish('mock', 'latest');
    }, 200);
  });
});

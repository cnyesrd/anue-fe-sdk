const Shared = anue.shared;

const ctx = Shared.createStatic<Anue.OTAC.OTACContext>('otac.ctx', {
  url: 'https://login.cnyes.com',
  engines: {},
  loaded: {},
  requests: {}
});

const OTACManager = {
  setServer: (url: string) => {
    ctx.get().server = url;
    return OTACManager;
  },
  loadComponent: async (
    id: string,
    version: string,
    replace: boolean = true
  ) => {
    if (!replace && ctx.get().loaded[id]) {
      return Shared.get(`otac:${id}.${version}`);
    }
    const ongoingRequest = anue['otac.ctx'].requests[`${id}.${version}`];
    if (!ongoingRequest) {
      await new Promise(resolve => {
        anue['otac.ctx'].requests[`${id}.${version}`] = resolve;
        anue['utils.res'].loadScript(`${ctx.get().server}/${id}.${version}.js`);
      });
    } else {
      await ongoingRequest;
      // delete anue['otac.ctx'].requests[`${id}.${version}`];
    }
    ctx.get().loaded[id] = version;

    return Shared.get(`otac:${id}.${version}`);
  },
  finish: (id: string, version: string, module: any) => {
    const pendingRequests: Anue.OTAC.OTACContext = anue['otac.ctx'].requests;
    if (pendingRequests[`${id}.${version}`]) {
      pendingRequests[`${id}.${version}`](module);
    }
    if (pendingRequests[`${id}.latest`]) {
      pendingRequests[`${id}.${version}`](module);
    }
  }
};

Shared.set('packages.otac', OTACManager);

export default Shared.get('packages.otac');

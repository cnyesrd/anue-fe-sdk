import RPC, { MockRPC } from '@utils/rpc';
import getty from '@utils/getty';
import waitty from '@utils/waitty';
import Shared from '@utils/shared';
import MockNetwork, { requestSpy } from '@mocks/mock-network';

jest.mock('@utils/rpc');
const mockRPC = mockAs<MockRPC>(RPC);

Shared.set('utils.rpc', mockRPC);
const Network = anue.shared.get('library.net');
import Auth from '../index';

const mockedProfile = {
  avatar: '@mock_avatar',
  email: 'mock@anue.com',
  identity_id: 'mock:id',
  name: '@mock_name',
  uid: 12345
};

let mockRPCStorage: Record<string, any> = {};

mockRPC.MockManager.set(async (method, args) => {
  const command = args.slice(0, 2);
  const api = command.join('.');
  let response = {};

  if (method === 'storage') {
    switch (api) {
      case 'set.profile':
      case 'set.sso':
        response = mockRPCStorage.get[args[1]] = args[2];
        break;
      case 'get.sso':
      case 'get.profile':
        response = getty(mockRPCStorage, command) || {};
        break;
      case 'delete.sso':
      case 'delete.profile':
        delete mockRPCStorage.get[args[1]];
        response = {};
        break;
      default:
        break;
    }
  }
  return { data: JSON.stringify(response) };
});

function prepare() {
  return {
    mockToken: (flag: string = '') => {
      mockRPCStorage.get.sso = { token: '$mock_token', flag };
      return prepare();
    },
    mockProfile: () => {
      mockRPCStorage.get.profile = mockedProfile;
      return prepare();
    },
    mockProfileAPI: (delay?: number) => {
      MockNetwork.mock(
        opts => opts.url.indexOf('/user/profile') > -1,
        () => ({
          items: {
            user: mockedProfile
          }
        }),
        delay
      );
      return prepare();
    },
    mockRefreshTokenAPI: (token, delay?: number) => {
      MockNetwork.mock(
        opts => opts.url.indexOf('/user/token/refresh') > -1,
        () => {
          return {
            items: {
              token
            }
          };
        },
        delay
      );
      return prepare();
    }
  };
}

function getTestInstance(imc: boolean = false, isOpener: boolean = false) {
  return new Auth({
    isOpener,
    imc
  });
}

function resetRPCStorage() {
  mockRPCStorage = {
    get: {
      sso: {},
      profile: {}
    }
  };
}

function setLocation(path = '/') {
  history.replaceState({}, 'mock', path);
}

beforeEach(() => {
  setLocation();
  resetRPCStorage();
  anue.urld = {};
  Network.clearInterceptors();
  MockNetwork.clearMocks();
  requestSpy.mockClear();
});

describe('offline Auth state persistence tests', () => {
  // it('should emit online event when RPC cache containing entries for access token and profile (fastest case)', done => {
  //   prepare()
  //     .mockProfile()
  //     .mockToken();

  //   // expecting that Auth will trigger login event
  //   getTestInstance()
  //     .onStateChange(ctx => {
  //       expect(ctx.status === 'online');
  //       // no API call is expected
  //       expect(requestSpy).toBeCalledTimes(0);
  //       done();
  //     })
  //     .consume();
  // });

  // it('should try to update profile if RPC cache contains only access token', done => {
  //   prepare()
  //     .mockToken()
  //     .mockProfileAPI();

  //   // expecting that Auth will trigger login event
  //   const auth = getTestInstance();
  //   auth
  //     .onStateChange(ctx => {
  //       expect(ctx.status === 'online');
  //       // a profile API call is expected
  //       expect(requestSpy).toBeCalledTimes(1);
  //       expect(auth.optimisticallyGetProfile()).toBe(mockedProfile);
  //       // should leave a copy of profile in RPC storage as well
  //       expect(mockRPCStorage.get.profile).toBe(JSON.stringify(mockedProfile));
  //       done();
  //     })
  //     .consume();
  // });

  it('should not emit `online` status if no token is found', async () => {
    const auth = getTestInstance();
    await auth.consume();
    expect(requestSpy).toBeCalledTimes(0);
    expect(auth.optimisticallyGetProfile()).toBe(null);
  });

  it('should be able to consume token from url', async () => {
    prepare().mockProfile();
    const token = Math.random();
    anue.urld = {
      token
    };
    const auth = getTestInstance();
    await auth.consume();
    const data = auth.getCredentialContext();

    expect(getty(data, ['authorization'])).toBe(token);
  });

  it('should be able to consume token from urld, hash, or query', async () => {
    prepare().mockProfile();
    const tests = [
      token => {
        anue.urld = {
          token
        };
        return token
      },
      token => {
        setLocation(`?token=${token}`);
        return token
      },
      token => {
        setLocation(`#token=${token}`);
        return token
      },
      token => {
        setLocation(`?q=search&token=${token}`);
        return token
      },
      token => {
        setLocation(`#q=search&token=${token}`);
        return token
      },
      () => {
        setLocation(`#faketoken=YOU_CONSUMED_FORGED_TOKEN_1`);
        return ''
      },
      () => {
        setLocation(`?q=1&xss=?token=YOU_CONSUMED_FORGED_TOKEN_2`);
        return ''
      },
      () => {
        setLocation(`?q=1&xss=##token=YOU_CONSUMED_FORGED_TOKEN_3&t=token=YOU_CONSUMED_FORGED_TOKEN_4`);
        return ''
      }
    ];

    for (const t in tests) {
      setLocation();
      resetRPCStorage();
      anue.urld = {};
      const token = Math.random();
      const expected = `${tests[t](token)}`;

      const auth = getTestInstance();
      await auth.consume();
      const data = auth.getCredentialContext();

      expect(`${getty(data, ['authorization'])}`).toBe(expected);
    }
  });
});

describe('offline state restoring tests', () => {
  it('should be able to update access token when encountering 401 status code', async () => {
    const mockToken = `$token_${Date.now()}`;
    prepare()
      .mockProfile()
      .mockToken()
      .mockRefreshTokenAPI(mockToken);

    MockNetwork.mock(
      opts => opts.url.indexOf('/user/test') > -1,
      (opts, extraRef) => {
        if (
          opts.headers &&
          opts.headers.authorization === `Bearer ${mockToken}`
        ) {
          extraRef.status = 200;
          return {
            message: 'OK'
          };
        }
        extraRef.status = 401;
        return {
          message: 'Unauthorized'
        };
      }
    );

    const auth = new Auth();
    await auth.consume();
    const result = await Network.getDriver().send({
      url: '/api/user/test'
    });
    expect(result).toEqual({
      message: 'OK'
    });
    const token = getty(auth.getCredentialContext(), ['authorization']);
    const result2 = await Network.getDriver().send({
      url: '/api/user/test'
    });
    expect(result2).toEqual({
      message: 'OK'
    });
    expect(token).toEqual(mockToken);
  });

  it('should should emit error when it failed to refresh access token', async done => {
    prepare()
      .mockProfile()
      .mockToken();
    MockNetwork.mock(
      opts => opts.url.indexOf('/api/unauthorized') > -1,
      (opts, extra) => {
        extra.status = 401;
        return {
          message: 'Unauthorized'
        };
      }
    );
    const auth = getTestInstance().onError(() => done());
    await auth.consume();
    await MockNetwork.send({
      url: '/api/unauthorized'
    });
  });

  it('should should emit error when it failed to refresh profile', async done => {
    const auth = getTestInstance().onError(() => done());
    await auth.consumeExisting({
      token: '$token'
    });
  });

  it('should preserve flag value when refreshing token', async () => {
    const flag = `${Date.now()}-${Math.random()}`;
    prepare()
      .mockProfile()
      .mockToken(flag);
    const auth = getTestInstance();

    await auth.consume();
    expect(getty(auth.getCredentialContext(), ['flag'])).toBe(flag);
    await auth.refreshToken();
    expect(getty(auth.getCredentialContext(), ['flag'])).toBe(flag);
  });
});

describe('race condition tests', () => {
  it('should properly remove data in local storage and rpc storage when log out', async done => {
    prepare()
      .mockProfile()
      .mockToken();
    const auth = getTestInstance();
    await auth.consume();
    auth.clearCredentials();
    setTimeout(() => {
      expect(mockRPCStorage.get.sso).not.toBeDefined();
      expect(mockRPCStorage.get.profile).not.toBeDefined();
      done();
    }, 100);
  });

  it('should not performs login when user logout before it login is finished', async () => {
    const token = `${Math.random()}-${Date.now()}`;
    prepare()
      .mockToken()
      .mockProfileAPI(50)
      .mockRefreshTokenAPI(token, 50);
    const auth = getTestInstance();
    // since the refreshing takes ~50ms, we can ensure the logout process fires before it finished
    await auth.consume();
    auth.logout();
    // wait for a little longer to ensure the state changes is finished, 100ms is a tolerable minimum
    await waitty(0);
    expect(getty(auth.getCredentialContext(), ['flag'])).toBeFalsy();
  });
});

describe('child frame login tests', () => {
  it('should be able to login with child frame event', async done => {
    const auth = getTestInstance(false, true);
    const token = Math.random();
    auth.onStateChange(ctx => {
      expect(getty(ctx, ['context', 'authorization'])).toBe(token);
      done();
    });
    window.postMessage(
      JSON.stringify({
        status: 'ok',
        token,
        flag: 'email'
      }),
      '*'
    );
  });

  it('malformed event should not take any effect', async () => {
    const auth = getTestInstance(false, true);
    const spy = jest.fn();
    auth.onStateChange(spy).onError(spy);
    window.postMessage(JSON.stringify(123), '*');
    await waitty(200);
    expect(spy).not.toBeCalled();
  });

  it('event with status=error should triggers emit error', async () => {
    const auth = getTestInstance(false, true);
    const changeSpy = jest.fn();
    const errorSpy = jest.fn();
    const data = Math.random();

    auth.onStateChange(changeSpy).onError(errorSpy);
    window.postMessage(
      JSON.stringify({
        status: 'error',
        data
      }),
      '*'
    );
    await waitty(1000);
    expect(changeSpy).not.toBeCalled();
    expect(errorSpy).toBeCalledTimes(1);
  });
});

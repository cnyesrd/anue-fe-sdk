/**
 * @anue-package Anue.Auth
 * @doc
 * Auth模組負責管理Token的交換、管理、刷新等環節。
 *
 * ## 範例 (Social與Email登入)
 *
 * ```Javascript
 * // use and bundle the SDK
 * import Auth from 'anue-fe-sdk/Auth'
 *
 * const auth = new Auth()
 *  .onStateChange(ctx => {
 *    console.log('Auth state changed', ctx)
 *  })
 * .consume()
 *
 *
 * ```
 */

const Shared = anue.shared;
const Endpoints = Shared.get('constants.endpoints');
const Keys = Shared.get('constants.keys');
const IMC = Shared.get('packages.imc');
const Network = Shared.get('library.net');
const waitty = Shared.get('utils.waitty');
const urly = Shared.get('utils.urly');
const getty = Shared.get('utils.getty');
const Eventy = Shared.get('utils.eventy');
const isServer = typeof document === 'undefined';

const refreshTokenEndpoints = [
  Endpoints.v1User.email.renew(),
  '/api/v1/user/token/refresh'
];

let backOff = -1;
let backoffExp = 0;
const backoffBase = 2;

/**
 * Dynamically get RPC host URL according to current page's host name.
 */
function getRPCHostByUrl() {
  let matchedHost = 'https://login.cnyes.com';

  if (typeof location === 'undefined') {
    return matchedHost;
  }

  // for debugging, when we find this one in local storage, use this one instead
  if (
    typeof localStorage !== 'undefined' &&
    localStorage['anue.auth.rpchost']
  ) {
    matchedHost = localStorage['anue.auth.rpchost'];
  } else {
    const host = location.host;
    const validHosts = [
      ['dev.int.cnyes.cool', 'https://login.int.cnyes.cool'],
      ['dev.beta.cnyes.cool', 'https://login.beta.cnyes.cool'],
      ['int.cnyes.cool', 'https://login.int.cnyes.cool'],
      ['beta.cnyes.cool', 'https://login.beta.cnyes.cool'],
      ['gamma.cnyes.cool', 'https://login.gamma.cnyes.cool'],
      ['stage.cnyes.cool', 'https://login.stage.cnyes.cool'],
      ['cnyes.com', 'https://login.cnyes.com'],
      ['int.withgod-test.anue.in', 'https://login.int.cnyes.cool'],
      ['beta.withgod-test.anue.in', 'https://login.beta.cnyes.cool'],
      ['stage.withgod-test.anue.in', 'https://login.stage.cnyes.cool']
    ];

    for (const i in validHosts) {
      if (host.indexOf(validHosts[i][0]) > -1) {
        matchedHost = validHosts[i][1];
        break;
      }
    }
  }

  return `${matchedHost}/dpi.html`;
}

let rpcChannel: Anue.Utils.RPC;
let iid = 0;

class Auth {
  static trackError = (error: { message: string; detail: any }) => {
    const tracker = Shared.get('extern.trackjs');
    if (tracker) {
      console.log('send error to TrackJS', error);
      tracker.error(error);
    }
  };
  public iid = iid++;
  private mHost = '/';
  private removeRequestInspector: (() => void) | null = null;
  private removeResponseInspector: (() => void) | null = null;
  private mProfile: Anue.Auth.UserProfile | null = null;
  private mSSOToken: string = '';
  private mFlag: string = '';
  private mStatus: Anue.Auth.AuthState = 'offline';
  private mRenewQueuePromise: Promise<any> | null = null;
  private mSessionId = -1;
  private tokenFormatter;
  private alwaysSendToken = false;
  private static getSSOData = async (): Promise<{
    token: string;
    flag: string;
  }> => {
    const rpc = await rpcChannel.rpcCall('storage', ['get', 'sso']);
    if (rpc.data) {
      return JSON.parse(rpc.data);
    }
    return { token: '', flag: '' };
  };

  private static removeSSOData = async () => {
    await rpcChannel.rpcCall('storage', ['delete', 'sso']);
  };

  private static cacheSSOData = async (token, flag) => {
    const json = JSON.stringify({
      token,
      flag
    });
    await rpcChannel.rpcCall('storage', ['set', 'sso', json]);
  };

  private static deSerializeAndValidateProfile = (json: string) => {
    try {
      return JSON.parse(json) as Anue.Auth.UserProfile;
    } catch (ignored) {}
    return null;
  };

  event = new Eventy();

  constructor(config: Anue.Auth.Config = {}) {
    const { host } = config;
    this.alwaysSendToken = !!config.alwaysSendToken;
    if (!rpcChannel && !isServer) {
      const RPC = Shared.get('utils.rpc');
      rpcChannel = new RPC('anue.auth', config.rpcHost || getRPCHostByUrl());
    }
    if (config.formatToken) {
      this.tokenFormatter = config.formatToken;
    }
    // @ts-ignore
    window._authToken = t => (this.mSSOToken = t);
    if (host) {
      this.mHost = host;
    }

    // do not attach message event when `isOpener` set to false
    if (config.isOpener !== false) {
      console.log('[Auth] listen to child frame events ..');
      window.addEventListener('message', this.listenChildFrameEvent);
    }

    if (config.imc !== false) {
      if (!IMC.has('auth.getCtx')) {
        IMC.register(
          'auth.stateChange',
          async (fn: EventyHandler<Anue.Auth.StateChangeEvent>) => {
            this.onStateChange(fn);
            return { data: null };
          }
        );
        IMC.register('auth.getCtx', async () => {
          const data = this.getCredentialContext();
          return await { data };
        });
        IMC.register(
          'auth.consume',
          async (args: { token: string; flag: string }) => {
            this.consumeExisting(args);
            return;
          }
        );
      } else if (DEBUG) {
        console.warn(
          // tslint:disable-next-line
          '[Auth] IMC method already registered, looks like you are creating multiple auth instance on the same page.\n' +
            'Be careful with this operation and its side-effect.'
        );
      }
    }
  }

  host = (host: string): Auth => {
    this.mHost = host;
    return this;
  };

  private isProfileValid = input => {
    return !!getty(input, ['identity_id']);
  };

  /**
   * Listen to events from all the frames to perform cross-frame login.
   * This method will automatically executed when `config.isOpener` is set to `true`.
   */
  private listenChildFrameEvent = async (e: MessageEvent) => {
    let data: {
      status: 'ok' | 'error';
      error?: any;
      token?: string;
      flag?: string;
    };

    try {
      try {
        data = JSON.parse(e.data);
      } catch (parseError) {
        return;
      }
      // console.log('[child event] %o', data, this.mConnectedProviders);
      if (data.status === 'ok' && data.token) {
        await this.commitLoginSuccess({
          token: data.token,
          flag: data.flag
        });
      } else if (data.status === 'error') {
        throw data;
      }
    } catch (error) {
      this.emitError({
        detail: {
          error,
          childEvent: e
        },
        message: '[auth] failed to login with child frame'
      });
    }
  };

  /**
   * @doc
   * @method onStateChange
   * @param fn EventyHandler<Anue.Auth.StateChangeEvent> 處理Auth狀態改變的函數，接受的參數是一個[Anue.Auth.StateChangeEvent]()物件
   * @returns [Auth](->Anue.Auth)
   * @public
   * 透過這個方法可以加入一個函數在Auth的狀態改變時被觸發，這也是應用端和Auth互動最主要的一個API。
   */
  onStateChange = (fn: EventyHandler<Anue.Auth.StateChangeEvent>): Auth => {
    this.event.on(Keys.Auth.Events.StateChange, fn);
    return this;
  };

  onError = (fn: EventyHandler<Anue.Auth.ErrorChangeEvent>) => {
    this.event.on(Keys.Auth.Events.Error, fn);
    return this;
  };

  private emitError = (error: { detail: any; message: string }) => {
    console.log('[Auth] Error', error);
    Auth.trackError(error);
    this.event.emit(Keys.Auth.Events.Error, {
      ...error,
      context: this.getCredentialContext()
    });
  };

  /**
   * @doc
   * @method emitStateChange
   * @private
   * This method emits state change event which fires all handlers added by `onStateChange` method.
   */
  private emitStateChange = async (status: Anue.Auth.AuthState) => {
    console.log('[Auth] dispatch state change event [%o]', status, this.iid);
    this.event.emit(Keys.Auth.Events.StateChange, {
      status,
      flag: this.mFlag,
      context: this.getCredentialContext()
    });
  };

  consumeExisting = (data: Anue.Auth.ExistingState) => {
    this.commitLoginSuccess(data);
    return this;
  };

  /**
   * Execute this method to consume the auth information in cookie storage.
   * Basically you'll need to use this method on pages that need auth.
   */
  consume = async (): Promise<boolean> => {
    let token = '';
    let flag = '';

    // check if urld storage contains token
    if (anue.urld && anue.urld.token) {
      console.log('check urld storage for tokens', anue.urld);
      token = anue.urld.token;
    }

    // token not found in urld storage and url, try to find from remote storage
    if (!token) {
      const storedToken = await Auth.getSSOData();
      console.log('check url stored tokens', storedToken);
      token = storedToken.token;
      flag = storedToken.flag;
    }

    // check if token is in the url (when the site doesn't apply urld)
    if (!token) {
      const matchedTokenUrl =
        /(^#|&)token\=[^\&]+/.exec(location.hash) ||
        /(^\?|&)token\=[^\&]+/.exec(location.search);

      console.log('check url hash and query for tokens', location.href);

      if (matchedTokenUrl) {
        token = matchedTokenUrl[0].split('=')[1];
      }
    }

    console.log(
      '%c[Auth] consume persisted state => %o',
      'background-color: #ffc700',
      { token, flag }
    );

    if (token) {
      await this.commitLoginSuccess({ token, flag });
    }
    return !!token;
  };

  /**
   * @doc
   * @private
   * @method requestNetworkInspector
   * >! 這個方法不為外部所引用
   * 這個方法會在認證步驟`renew`或者`consume`完成之後自動由`Auth`添加至`Network`的
   * `requestInspector`佇列之中，此後每當有任何請求時`authorization`會被自動加入在
   * 請求的header中。
   */
  private requestNetworkInspector: Anue.Network.RequestInterceptor = async (
    options: Anue.Network.RequestOptions
  ) => {
    if (this.mSSOToken && (options.auth || this.alwaysSendToken)) {
      options.headers = options.headers || {};
      options.headers.authorization =
        options.headers.Authorization ||
        options.headers.authorization ||
        `Bearer ${this.mSSOToken}`;

      if (this.tokenFormatter) {
        options.headers.authorization = this.tokenFormatter(
          options,
          options.headers.authorization
        );
      }
    }
    return await options;
  };

  /**
   * @doc
   * @private
   * @method responseNetworkInspector
   * >! 這個方法不為外部所引用
   * 這個方法會在認證步驟`renew`或者`consume`完成之後自動由`Auth`添加至`Network`的
   * `responseInspector`佇列之中，之後當請求回應狀態代碼為`401`時，這個方法會自動呼叫
   * [Auth.renew](#renew)並且試著將原本的請求**重送**。
   */
  private responseNetworkInspector: Anue.Network.ResponseInterceptor = async (
    options: Anue.Network.RequestOptions,
    response: Anue.Network.Response,
    extra
  ) => {
    const { url } = options;
    const statusCode = getty(extra, ['status']);
    const retries = getty(options, ['extra', 'resent']);
    const isNeedRefresh =
      statusCode === 401 &&
      // do not refresh token on 401 if the endpoint is in the blacklist
      refreshTokenEndpoints.reduce(
        (p, c) => p || url.indexOf(c) > -1,
        false
      ) === false;
    if (retries || !isNeedRefresh || !this.mSSOToken) {
      return await response;
    }
    console.log('got 401 request options = %o', options);
    console.log(
      '%c[Auth] a 401 response captured, interceptor is trying to renew the credentials ..',
      'color: red;'
    );
    let credentials;
    // try to refresh when 401 or refresh token failed
    credentials = await this.renew('Auth.Interceptor');

    // if the credentials successfully updated, resend original request
    // otherwise just throw the error
    if (credentials) {
      options.extra = {
        ...options.extra,
        resent: true
      };
      // after the credentials refreshed, extend original options with new inspector
      const correctedOptions = await this.requestNetworkInspector(options);
      response = await Network.getDriver().send({
        ...correctedOptions,
        headers: {
          ...correctedOptions.headers,
          authorization: `Bearer ${this.mSSOToken}` || ''
        }
      });
    }

    return await response;
  };

  /**
   * @doc
   * @public
   * @promise
   * @method renew
   * @param ref? A string flag for tracking renew source.
   * @returns Anue.Auth.Credentials | null
   * This method prevents unnecessary renew request, concurrent renew requests will use the result
   * from the first request.
   */
  renew = async (ref?: any): Promise<Anue.Auth.Credentials | null> => {
    let result = null;
    try {
      if (this.mRenewQueuePromise) {
        console.log('[Auth] renew request is deferred');
      }
      if (this.mRenewQueuePromise === null) {
        this.mRenewQueuePromise = this.quableRenew(ref);
      }
      result = await this.mRenewQueuePromise;
      if (this.mRenewQueuePromise) {
        this.mRenewQueuePromise = null;
      }

      if (!result) {
        throw {
          message: '[Auth] failed to renew token',
          detail: result
        };
      }
    } catch (error) {
      this.emitError(error);
    }
    return result;
  };

  /**
   * This is the place which renew actually happens, with a logarithmic back-off timer.
   */
  quableRenew = async (ref?: any): Promise<Anue.Auth.Credentials | null> => {
    backOff = Date.now() + Math.pow(backoffBase, backoffExp) * 1000;

    // exponential backoff, prevent application layer calling this in high frequency
    if (backoffBase && Date.now() < backOff) {
      const waitFor = Math.pow(backoffBase, backoffExp++) * 1000;
      console.log('performing exponential backoff .. %s ms', waitFor);
      await waitty(waitFor);
    }
    backOff = -1;
    backoffExp = 1;

    console.log('[Auth] renew called ref = %s', ref);

    const response: Anue.Auth.RefreshTokenResponse = await this.refreshToken();
    console.log('[Auth] refreshToken response = %o', response);
    const token = getty(response, ['items', 'token']);

    if (token) {
      await this.commitLoginSuccess({ token, flag: this.mFlag || '' });
    } else {
      this.emitError({
        message: 'Failed to renew access token, invalid response',
        detail: response
      });
    }

    return this.getCredentialContext();
  };

  refreshToken = async () => {
    return await Network.getDriver().send<Anue.Auth.RefreshTokenResponse>({
      method: 'POST',
      url: urly(this.mHost, `/api/v1/user/token/refresh`),
      headers: {
        authorization: `Bearer ${this.mSSOToken}`
      }
    });
  };

  /**
   * This should probably used when user does not provide email.
   */
  updateEmail = async (email: string) => {
    // update email in database
    const result = await Network.getDriver().send({
      method: 'POST',
      url: urly(this.mHost, '/api/v1/user/profile/completeEmail'),
      headers: {
        authorization: `Bearer ${this.mSSOToken}`
      },
      body: {
        email
      }
    });

    if (result.nativeStatus >= 400) {
      return { error: result };
    }

    // update cached profile so it's consistent with the one in database
    if (this.mProfile) {
      this.mProfile.email = email;
      await rpcChannel.rpcCall('storage', [
        'set',
        'profile',
        JSON.stringify(this.mProfile)
      ]);
    }
    return {};
  };

  optimisticallyGetProfile = () => {
    if (!this.mProfile) {
      try {
        const stored = JSON.parse(localStorage['anue.profile']);
        this.mProfile = stored;
      } catch (ignored) {}
    }
    return this.mProfile;
  };

  private syncProfileFromLocalStorage = async (): Promise<Anue.Auth.UserProfile | null> => {
    let profile: Anue.Auth.UserProfile | null = null;
    console.log(
      '[Auth] parse profile in localStorage (fastest cache)',
      localStorage['anue.profile']
    );
    profile = Auth.deSerializeAndValidateProfile(localStorage['anue.profile']);
    return profile;
  };

  private syncProfileFromRPCStorage = async (): Promise<Anue.Auth.UserProfile | null> => {
    let profile: Anue.Auth.UserProfile | null = null;
    console.log('[Auth] check profile in dpi cache (slower)');
    const rpcCachedProfile = await rpcChannel.rpcCall('storage', [
      'get',
      'profile'
    ]);
    profile = Auth.deSerializeAndValidateProfile(
      getty(rpcCachedProfile, ['data'])
    );
    return profile;
  };

  private syncProfileFromRemote = async (): Promise<Anue.Auth.UserProfile | null> => {
    let profile: Anue.Auth.UserProfile | null = null;
    if (!this.mSSOToken) {
      console.log(
        '[Auth] get remote profile failed, no authorization token found.',
        null
      );
      return profile;
    }
    if (!this.isProfileValid(this.mProfile)) {
      console.log(
        '[Auth] get profile from API as local caches missed (slowest)'
      );
      const response = await Network.getDriver().send<
        APIResponse.ServiceResponse<{ user: Anue.Auth.UserProfile }>
      >({
        url: `${this.mHost}/api/v1/user/profile`,
        headers: {
          authorization: `Bearer ${this.mSSOToken}`
        }
      });

      profile = getty(response, ['items', 'user']);
      if (!this.isProfileValid(profile)) {
        console.log(
          '[Auth] getProfile failed, profile does not contain an email.',
          profile
        );
        return null;
      }
    }
    return profile;
  };

  /**
   * A method which try to get user's profile by any means. This method
   * is invoked every time Auth renew user's credential.
   */
  syncProfile = async (
    nextSessionId
  ): Promise<Anue.Auth.UserProfile | null> => {
    const attempts = [
      this.syncProfileFromLocalStorage,
      this.syncProfileFromRPCStorage,
      this.syncProfileFromRemote
    ];
    let result: Anue.Auth.UserProfile | null = null;
    let isValidProfileFetched = false;

    // Get profile from storage and remote API, use the first success response
    for (const i in attempts) {
      result = await attempts[i]();
      isValidProfileFetched = this.isProfileValid(result);
      if (isValidProfileFetched) {
        break;
      }
    }

    try {
      // Sometimes the login status changes during the sync phase, here we check the
      // status again to ensure that client is still logged in.
      if (
        isValidProfileFetched &&
        this.mStatus === 'online' &&
        nextSessionId === this.mSessionId
      ) {
        console.log('[Auth] commit updated profile =', result);
        this.mProfile = result;
        await rpcChannel.rpcCall('storage', [
          'set',
          'profile',
          JSON.stringify(this.mProfile)
        ]);
      } else {
        this.emitError({
          message: 'Failed to update profile, invalid response.',
          detail: result
        });
      }
    } catch (err) {
      this.emitError({
        message: 'Failed to update profile, rpc failed.',
        detail: err
      });
    }

    return this.mProfile;
  };

  login = (method: 'google' | 'facebook' | 'email') => {
    // TODO: change API endpoint
    // @ts-ignore
    location = `${this.mHost}/login`;
  };

  /**
   * @doc
   * @method commitLoginSuccess
   * @private
   * 這個方法用來更新儲存當前的登入狀態，在設計上只有當使用者透過某個Provider成功登入後才會觸發這個方法。
   * 所有的成功登入後所需要處理的事情都只能寫在這裡。
   */
  private commitLoginSuccess = async (data: {
    token: string;
    identityId?: string;
    flag?: string;
  }): Promise<Auth> => {
    const { token, flag } = data;
    const nextSessionId = Math.random() + Date.now();
    // create a new session ID when we're about to change the authentication context
    this.mSessionId = nextSessionId;
    console.log(
      '[Auth] setCredentials %o session = %o current_session = %o',
      { token },
      nextSessionId,
      this.mSessionId
    );

    if (!token) {
      // cache profile for an hour
      return this;
    }
    this.mFlag = flag || '';
    this.mSSOToken = data.token;
    this.mStatus = 'online';

    // add inspector for handling API authorization
    if (!this.removeRequestInspector) {
      this.removeRequestInspector = Network.addRequestInterceptor(
        this.requestNetworkInspector
      );
    }
    if (!this.removeResponseInspector) {
      this.removeResponseInspector = Network.addResponseInterceptor(
        this.responseNetworkInspector
      );
    }

    console.log(
      '%c[Auth] %cstate changed %conline',
      'font-weight:bolder;background-color: #b3ef99;',
      'background-color: #b3ef99;',
      'font-weight:bolder;background-color: #b3ef99'
    );

    if (nextSessionId === this.mSessionId) {
      // save the information at once
      await Promise.all([
        Auth.cacheSSOData(this.mSSOToken, this.mFlag),
        this.syncProfile(nextSessionId)
      ]);
    }

    this.emitStateChange('online');

    return this;
  };

  /**
   * @doc
   * @method clearCredentials
   * 清除所有儲存的認證資訊，等同於`logout`。當這個Auth物件有使用[connectProvider](->Anue.Auth#connectProvider)
   * 連結其他[Provider](->Anue.AbstractProvider)這個方法會在連結的Provider呼叫`logout`時一併被呼叫。
   */
  clearCredentials = () => {
    this.mSSOToken = '';
    this.mProfile = null;

    // remove network auth inspector when credentials are cleared
    if (this.removeRequestInspector) {
      this.removeRequestInspector();
    }
    if (this.removeResponseInspector) {
      this.removeResponseInspector();
    }
    this.removeRequestInspector = null;
    this.removeResponseInspector = null;

    delete localStorage['anue.profile'];
    rpcChannel.rpcCall('storage', ['delete', 'profile']);
    Auth.removeSSOData();

    console.log(
      '%c[Auth] %cstate changed %coffline',
      'font-weight:bolder;background-color: #fff693;',
      'background-color: #fff693;',
      'font-weight:bolder;background-color: #fff693;'
    );
    this.emitStateChange('offline');
    // window.removeEventListener('message', this.listenChildFrameEvent);
    this.mSessionId = -1;

    return this;
  };

  // alias method
  logout = this.clearCredentials;

  /**
   * Get last stored credential context object
   */
  getCredentialContext = (): Anue.Auth.Credentials | null => {
    return {
      authorization: this.mSSOToken,
      flag: this.mFlag,
      profile: this.mProfile
    };
  };
}

Shared.set('packages.auth', Auth);

export default Auth;

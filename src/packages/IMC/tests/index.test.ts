import IMC from '@packages/IMC';
import Shared from '@utils/shared';

describe('IMC tests', () => {
  it('IMC.register test', () => {
    const handle = jest.fn();
    // @ts-ignore
    IMC.register(
      'test',
      // @ts-ignore
      handle
    );

    expect(Shared.get('anue.imc:test')).toBe(handle);
  });

  it('IMC.call test', async () => {
    const handle = async (val: string) => await { data: val };
    IMC.register('test.call', handle);

    const expected = `${Date.now()}`;
    // @ts-ignore
    const actual = await IMC.call('test.call', expected);
    expect(actual.data).toBe(expected);
  });

  it('pending unregister function calls', done => {
    const id = `imc:test-${Math.random()}`;
    const spy = jest.fn();
    const calls = Math.random() * 20;
    const promises = [];
    for (let i = 0; i < calls; i++) {
      // @ts-ignore
      promises.push(IMC.call(id));
    }
    Promise.all(promises).then(() => {
      expect(spy).toBeCalledTimes(Math.ceil(calls));
      done();
    });
    setTimeout(() => {
      expect(spy).not.toBeCalled();
      IMC.register(id, spy);
    }, 50);
  });
});

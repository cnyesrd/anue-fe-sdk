/**
 * IMC (inter-module communication)模組主要是用藉由封裝過後的SharedContext介面
 * 和型別定義讓OTA模組可以互相溝通。
 */
const Shared = anue.shared;

type IMCFunctionCall = (args: any) => Promise<Anue.IMC.Response<any>>;
type QueuedIMCFunctionCall = (f: IMCFunctionCall) => void;

const queue = Shared.createStatic<Record<string, QueuedIMCFunctionCall[]>>(
  'imc.queue',
  {}
);

const IMC = {
  /**
   * 註冊一個`跨context共用`的IMC方法，在定義這個方法後請到`imc.d.ts`中將其多載型別加入其中，
   * 以便不同產品使用時可以直接透過autocomplete來檢視其使用方式。
   */
  register: (name: string, handle) => {
    const p = `anue.imc:${name}`;
    const q = queue.get()[p];

    Shared.set(p, handle);
    if (q) {
      q.forEach(f => f(handle));
    }
  },

  has: (method: string): boolean => {
    return !!Shared.get(`anue.imc:${method}`);
  },

  /**
   * 呼叫一個IMC方法，在這邊的型別定義並沒有強制指定args和返回的型別，然而在使用上必須對其參數以及返回型別
   * 在`imc.d.ts`中加以定義，否則在第三方使用端無法通過typescript的檢查。
   */
  call: (): Promise<Anue.IMC.Response<any>> | void => {
    const args: any[] = [];
    // @ts-ignore
    for (const k in arguments) {
      // @ts-ignore
      args.push(arguments[k]);
    }
    // @ts-ignore
    console.log('args %o => %o', arguments, args);

    return new Promise((resolve, reject) => {
      const method = `anue.imc:${args[0]}`;
      const fn = Shared.get<(...args: any[]) => any>(method);
      // method not exists, queueing for method register
      if (!fn) {
        if (!queue.get()[method]) {
          queue.get()[method] = [];
        }

        queue.get()[method].push(f => {
          // @ts-ignore
          resolve(f.apply(null, args.slice(1)));
        });
      } else {
        resolve(fn.apply(null, args.slice(1)));
      }
    });
  }
};

Shared.set('packages.imc', IMC);

export default IMC;

// tslint:disable
type URLBuilder = { (): string; __path: string };

function define(ctx: URLBuilder, ...p) {
  p.forEach(p => {
    Object.defineProperty(ctx, p, {
      get() {
        ctx.__path += '/' + p;
        return ctx;
      }
    });
  });
  return ctx;
}

// @ts-ignore
class Endpoints {
  static get api(): Anue.ValidEndpoints {
    // @ts-ignore
    let ctx = function() {
      // @ts-ignore
      return this.__path;
    };
    // @ts-ignore
    ctx.__path = '/api';
    // @ts-ignore
    return define(ctx, 'v1', 'user', 'registration', 'email', 'cognitoOpenIdOrRegister', 'renew', 'verify', 'tos', 'all', 'profile', 'v2', 'resend', 'password', 'recovery', 'reset', 'new', 'avatar', 'notifications', 'redirect', 'notification', 'read', 'unread');
  }
  static get v1User() {
    return Endpoints.api.v1.user;
  }
}

anue.shared.set('constants.endpoints', Endpoints);

// @ts-ignore
export default Endpoints;
// tslint:enable

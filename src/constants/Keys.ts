export enum SupportedProviderType {
  Social = 'social',
  Email = 'email'
}

export enum SupportedChannel {
  Stock = 'stock',
  MobileApp = 'mobile-app',
  Crypto = 'crypto',
  Driver = 'driver',
  Unknown = 'unknown'
}

const Keys = {
  Auth: {
    Events: {
      StateChange: 1,
      Update: 2,
      Error: 3,
      Loaded: 4
    },
    Storage: {
      CognitoId: 'anue.cognitoId',
      IdToken: 'anue.idToken',
      AccessToken: 'anue.accessToken',
      Authorization: 'anue.authorization',
      NewComer: 'anue.newcomer',
      Provider: 'anue.provider',
      Profile: 'anue.profile',
      SSOProvider: 'anue.sso'
    }
  },
  Debuggable: {
    Config: 'anue.debuggable.config',
    Logger: 'anue.debuggable.logger'
  },
  Storage: {
    Driver: 'anue.storage.driver',
    Abstract: 'anue.storage.abstract'
  },
  Network: {
    Driver: 'anue.network.driver',
    Abstract: 'anue.network.abstract'
  },
  SupportedChannel,
  SupportedProviderType
};

anue.shared.set('constants.keys', Keys);

export default Keys;

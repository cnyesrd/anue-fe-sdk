import Endpoints from '@constants/Endpoints';

describe('Endpoints builder tests', () => {
  it('should return correct result', () => {
    expect(Endpoints.api.v1.user.cognitoOpenIdOrRegister()).toBe(
      '/api/v1/user/cognitoOpenIdOrRegister'
    );

    expect(Endpoints.api.v1.user.registration.email()).toBe(
      '/api/v1/user/registration/email'
    );
  });
});

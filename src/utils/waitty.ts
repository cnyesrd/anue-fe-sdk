const waitty = time =>
  new Promise(resolve => {
    const tm = setTimeout(() => {
      clearTimeout(tm);
      resolve();
    }, time);
  });

anue.shared.set('utils.waitty', waitty);

export default waitty;

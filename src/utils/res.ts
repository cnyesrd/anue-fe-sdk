export const loadCSS = (url, id?: any) =>
  new Promise((resolve, reject) => {
    if (typeof document !== 'undefined') {
      if (id) {
        const prev = document.querySelector(`#${id}`);
        if (prev && prev.parentNode) {
          prev.parentNode.removeChild(prev);
        }
      }
      const css = document.createElement('link');
      if (id) {
        css.id = id;
      }
      css.rel = 'stylesheet';
      css.href = url;
      css.onload = resolve;
      css.onerror = reject;
      document.head.appendChild(css);
    }
  });

export const loadScript = url =>
  new Promise((resolve, reject) => {
    if (typeof document !== 'undefined') {
      const s = document.createElement('script');
      s.src = url;
      s.onload = resolve;
      s.onerror = reject;
      document.head.appendChild(s);
    }
  });

const res = {
  loadCSS,
  loadScript
};

anue.shared.set('utils.res', res);

export default res;

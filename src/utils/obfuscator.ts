const obfuscate = (input: string, key = 20): string => {
  const chars = input.split('');

  for (let i = 0; i < chars.length; i++) {
    const c = chars[i].charCodeAt(0);

    if (c <= 126) {
      chars[i] = String.fromCharCode((chars[i].charCodeAt(0) + key) % 126);
    }
  }

  return chars.join('');
};

const defuscate = (input: string): string => {
  return obfuscate(input, 106);
};

anue.shared.set('utils.obfuscator', { obfuscate, defuscate });

export { obfuscate, defuscate };

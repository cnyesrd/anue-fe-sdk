import urly from '@utils/urly';

describe('urly tests', () => {
  it('should remove consecutive slashes', () => {
    expect(urly('////a//b/c')).toBe('/a/b/c');
  });

  it('should not remove double slashes after protocol', () => {
    expect(urly('http://a.b.c/abc')).toBe('http://a.b.c/abc');
  });
});

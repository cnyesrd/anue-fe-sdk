import Eventy from '@utils/eventy';

describe('eventy tests', () => {
  it('basic emit tests', () => {
    const e = new Eventy();
    const handlers: any[] = [];

    for (let i = 0; i < 100; i++) {
      const f = jest.fn();
      handlers.push(f);
      e.on('test', f);
    }
    const now = Date.now();
    e.emit('test', now);

    handlers.forEach(h => {
      expect(h).toBeCalledWith(now);
    });
  });

  it('remove handler emit tests', () => {
    const e = new Eventy();
    const handlers: any[] = [];
    let expected = 0;
    let actual = 0;

    for (let i = 0; i < 100; i++) {
      const f = () => actual++;
      handlers.push(f);
      if (Math.random() > 0.5) {
        expected++;
        e.on('test', f)();
      } else {
        e.on('test', f);
      }
    }
    const now = Date.now();
    e.emit('test', now);

    expect(100 - expected).toBe(actual);
  });
});

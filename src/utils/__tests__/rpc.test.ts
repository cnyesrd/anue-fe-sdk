import MockEventy, { handleEmitType, reset } from '@mocks/mock-eventy';

anue['utils.eventy'] = MockEventy;
import waitty from '@utils/waitty';
import rpc from '../rpc';

beforeEach(() => {
  reset();
});

describe('rpc tests', () => {
  it('should create an iframe when channel created', () => {
    const url = `rpc://${Math.random()}`;
    new rpc('test', url);

    const agent = document.querySelector('iframe');

    expect(agent).toBeDefined();
    if (agent) {
      expect(agent.src).toBe(url);
    }
  });

  it('should ignore events from domain not under cnyes', async () => {
    new rpc('test-xss', 'xss://');
    await waitty(100);
    window.postMessage(
      {
        data: JSON.stringify({
          tid: 'xss',
          type: 'xss'
        }),
        origin: 'xss://pm'
      },
      '*'
    );
    const spy = jest.fn();
    handleEmitType('xss', spy);
    await waitty(100);
    expect(spy).not.toBeCalled();
  });
});

/**
 * @anue-utils Anue.Utils
 * @doc
 * 開發SDK所使用的工具集合，在一般應用程式開發的時候較不會被使用
 *
 * [fn:load_fragment](utils-getty)
 */

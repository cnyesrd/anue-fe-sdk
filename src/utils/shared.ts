/**
 * @doc
 * @anue-base Anue.SharedContext
 * @private
 * `SharedContext`是一個用來處理內部跨模組資料交換的模組，主要用來解決不同的SDK模組在透過
 * webpack封裝後共用變數的存取問題。同時這個模組也提供一個在不同context間溝通時的基本儲存空間。
 */

let scope: any = null;

if (typeof window === 'undefined') {
  // @ts-ignore
  scope = global;
} else {
  scope = window;
}

if (!scope.anue) {
  scope.anue = {};
}

const Shared = {
  set: (key, value) => {
    scope.anue[key] = value;
  },
  get: <T>(key) => {
    return scope.anue[key] as T;
  },
  createStatic: <T>(key, val): Anue.Shared.SharedStatic<T> => {
    const hash = `static:${key}`;
    if (!scope.anue[hash]) {
      scope.anue[key] = val;
      scope.anue[hash] = {
        get() {
          return scope.anue[key];
        },
        set(v) {
          scope.anue[key] = v;
        }
      };
    }
    return scope.anue[hash] as Anue.Shared.SharedStatic<T>;
  }
};

scope.anue.shared = Shared;

export default Shared;

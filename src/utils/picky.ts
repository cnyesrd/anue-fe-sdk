export default function<T = any>(obj: any, args: Array<number | string>): T {
  if (!Array.isArray(args)) {
    return {} as T;
  }
  return args.reduce((p, c) => {
    const val = obj[c];
    if (val) {
      p[c] = val;
    }
    return p;
  }, {}) as T;
}

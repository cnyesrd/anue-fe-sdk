export function toObject(url: string) {
  let str = url.split('?')[1];
  let result = {};
  if (str) {
    result = str.split('&').reduce((p, c) => {
      const [k, v] = c.split('=');
      p[k] = v;
      return p;
    }, {});
  }
  return result;
}

const paramly = function(params) {
  let str = '';
  for (const p in params) {
    const leading = str ? '&' : '?';
    str += `${leading}${encodeURIComponent(p)}=${encodeURIComponent(
      params[p]
    )}`;
  }
  return str;
};

paramly.toObject = toObject;

anue.shared.set('utils.paramly', paramly);

export default paramly;

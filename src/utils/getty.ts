/**
 * @anue-fragment utils-getty
 * @doc
 * getty是一個簡單的`get`函數，用途同於`lodash.get`或者`idx`不過更為輕量。
 * 遵循以下原則：
 * 給定目標物件`O`和路徑`p`，對於`O`進行一個`reduce`的遞迴
 * - 當`O`存在則返回`O[p]`
 * - 當`O`是空值(`null` | `undefined`)則返回`O`
 *
 * ```javascript
 * import getty from '@utils/getty'
 *
 * getty(undefined)
 * // undefined
 * getty({ a: { b: { c: 100 } } }, ['a', 'b', 'c'])
 * // 100
 * getty({ foo: null }, ['foo'])
 * // null
 *
 * ```
 *
 * @param target 目標物件
 * @param path 目標值所存在的路徑，必須是一個`Array`
 */

function getty<T = any>(
  target: any,
  path: Array<number | string>
): T | undefined {
  return Array.isArray(path)
    ? path.reduce((p, c) => p && p[c], target)
    : target === undefined || target === null
    ? target
    : target[path];
}

anue.shared.set('utils.getty', getty);

export default getty;

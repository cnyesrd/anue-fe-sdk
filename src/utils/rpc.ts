const Shared = anue.shared;
const Eventy = Shared.get('utils.eventy');
const event: Anue.Utils.Eventy = new Eventy();

export interface MockRPC {
  new (): RPC;
  MockManager: {
    /**
     * A method which handles mocking rpc requests, the response data must be an
     * object contains a field `data` which is string of JSON response of the RPC call.
     */
    set: (
      fn: (method: string, args: any[]) => Promise<{ data: string }>
    ) => void;
  };
}

class RPC {
  private mProxyUri: string = '';
  // @ts-ignore
  private mDpi: HTMLIFrameElement;
  private mTag: string;
  private mTid: number = 1;
  private mProxyReady: boolean = false;
  private mProxyReadyPromise: Promise<any> | null = null;

  constructor(tag: string, proxyUri: string) {
    if (typeof window !== 'undefined') {
      window.addEventListener('message', e => {
        if (
          e.origin.indexOf('cnyes.com') > -1 ||
          e.origin.indexOf('cnyes.cool') > -1
        ) {
          try {
            const data = JSON.parse(e.data);
            if (data.type) {
              const eventToken = data.tid;
              event.emit(eventToken, data);
            }
          } catch (ignored) {}
        }
      });
    }

    if (!tag || !proxyUri) {
      throw Error(
        `[rpc] can not be initialized with tag=${tag} proxyUri=${proxyUri}`
      );
    }

    this.mProxyUri = proxyUri;
    this.mTag = tag;

    const frameId = `anue-dpi-${this.mTag}`;
    let frame = document.getElementById(frameId) as HTMLIFrameElement;
    if (!frame) {
      this.mProxyReadyPromise = new Promise((resolve, reject) => {
        frame = document.createElement('iframe');
        frame.src = `${this.mProxyUri}`;
        frame.setAttribute('id', frameId);
        frame.setAttribute(
          'style',
          'border:0;position:fixed;left:-9999px;top:-9999px;height:1px;width:1px'
        );
        frame.onload = () => {
          this.mProxyReady = true;
          resolve();
        };
        frame.onerror = reject;
        this.mDpi = frame;
        document.body.appendChild(this.mDpi);
      });
    }
  }

  private waitForProxy = async (ref?: string): Promise<void> => {
    if (this.mProxyReady) {
      return Promise.resolve();
    }
    return this.mProxyReadyPromise;
  };

  rpcCall = (prefix, args: any[]): Promise<Anue.DPI.Payload> => {
    return new Promise<Anue.DPI.Payload>(async (resolve, reject) => {
      await this.waitForProxy(prefix + ':' + args.join('.'));
      try {
        ++this.mTid;
        // TODO: better way to detect rpc fail
        const tm = setTimeout(() => {
          reject({
            token: `dpi:${this.mTag}.${this.mTid}`,
            message: '[rpc] failed to load due to request timeout.',
            detail: { prefix, ...args }
          });
          clearTimeout(tm);
        }, 10000);
        const eventToken = `dpi:${this.mTag}.${this.mTid}`;
        const revoke = event.on<Anue.DPI.Payload>(
          eventToken,
          (data: Anue.DPI.Payload) => {
            clearTimeout(tm);
            if (!data.error) {
              resolve(data);
            } else {
              reject(data);
            }
            revoke();
          }
        );
        // @ts-ignore
        this.mDpi.contentWindow.postMessage(
          {
            tid: eventToken,
            method: prefix,
            args
          },
          '*'
        );
      } catch (err) {
        reject(err);
      }
    });
  };
}

Shared.set('utils.rpc', RPC);

export default RPC;

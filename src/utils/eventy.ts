export type EventyHandler<T = any> = (data: T) => void;
export type EventyRemover = () => void;

class Eventy {
  handlers = {};

  clear = () => (this.handlers = {});

  emit = (eventName: string | number, data?: any) => {
    const fns = this.handlers[eventName];
    if (fns) {
      fns.forEach(fn => fn(data));
    }
  };

  on = <T = any>(
    eventName: string | number,
    fn: EventyHandler<T>
  ): EventyRemover => {
    this.handlers[eventName] = this.handlers[eventName] || [];
    this.handlers[eventName].push(fn);
    return () => {
      const index = this.handlers[eventName].indexOf(fn);
      if (index > -1) {
        this.handlers[eventName].splice(index, 1);
      }
      // @ts-ignore
      fn = null;
    };
  };
}

anue.shared.set('utils.eventy', Eventy);

export default Eventy;

const Shared = anue.shared;
const init = Shared.createStatic('urld.init', false);
const pat = /[^\=#\?\&]+\=[^&#]+/g;

const urld = {
  parse: () => {
    if (typeof location !== 'undefined') {
      const matchesHash = location.hash.match(pat) || [];
      const matchesQuery = location.search.match(pat) || [];
      const data = {};
      const matches = matchesHash.concat(matchesQuery);

      if (matches) {
        console.log('[urld] matches = %o', matches);
        matches.forEach(m => {
          const pairs = m.split('=');
          data[pairs[0].replace('anue_', '')] = decodeURIComponent(pairs[1]);
          console.log(
            'urld set %s => %s',
            pairs[0].replace('anue_', ''),
            decodeURIComponent(pairs[1])
          );
        });
      }
      return data;
    }
    return {};
  },
  init: () => {
    if (typeof location !== 'undefined' && !init.get()) {
      init.set(true);
      const result = urld.parse();

      Shared.set('urld', result);

      console.log(
        '[urld] consumed hashes and queries as object in SharedContext %o',
        result
      );
      let url =
        location.protocol +
        '//' +
        location.hostname +
        ':' +
        location.port +
        location.pathname +
        location.hash.replace(pat, '');
      url = url.replace(/&+/g, '');
      history.pushState(null, document.title, url);
    }
  },
  get: <T = any>(key): T => ((Shared.get('urld') as T) || {})[key]
};

Shared.set('utils.urld', urld);

export default urld;

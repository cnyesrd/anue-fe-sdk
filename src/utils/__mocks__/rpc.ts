
export default class MockRPC {
  static MockManager = {
    resolve: (method: string, args: any[]) => {},
    set: fn => {
      MockRPC.MockManager.resolve = fn;
    }
  }
  rpcCall = async (method: string, args: any[]) => {
    console.log('mock RPC called', method, args)
    return await MockRPC.MockManager.resolve(method, args);
  }
}

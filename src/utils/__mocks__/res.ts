const mocks = {
  css: {},
  script: {}
};

export function resetMock() {
  mocks.css = {};
  mocks.script = {};
}

export function mockCSS(url, fn) {
  mocks.css[url] = fn;
  return () => {
    mocks.css[url] = undefined;
  };
}

export function mockScript(url, id, fn) {
  mocks.script[url + id] = fn;
  return () => {
    mocks.script[url + id] = undefined;
  };
}

export const loadCSS = async (url: string) => {
  if (mocks.css[url]) {
    mocks.css[url](url);
  }
}

export const loadScript = async (url: string, id?: string) => {
  if (mocks.script[url]) {
    mocks.script[url](url);
  }
}

const { obfuscate, defuscate } = anue.shared.get('utils.obfuscator');
const isBrowser = typeof document !== 'undefined';
const secret = [119, 5, 5, 1, 125, 121];
const key = secret.reduce((p, c) => p + String[defuscate('zW|uWxy')](c), '');
const domain =
  typeof location === 'undefined' ? '.beta.cnyes.cool' : location.hostname;
const match = /\.(beta\.|int\.|stage\.)?(cnyes\.(cool|com)|anue\.(in|com))\/?$/.exec(
  domain
);
const matchedDomain = match ? match[0] : '/';

const getDocumentCookiesObfuscated = () => {
  if (typeof document === 'undefined') {
    return '';
  }
  const doc = atob('ZG9jdW1lbnQ=');
  return window[doc][defuscate(key)];
};

const createCookie = (
  name: string,
  data: any,
  maxAge: number,
  shouldEncode: boolean = true
) => {
  if (matchedDomain && isBrowser) {
    console.log('create cookie', name, data, matchedDomain);
    document.cookie = `anue.${name}=${
      shouldEncode
        ? btoa(obfuscate(JSON.stringify(data)))
        : JSON.stringify(data)
    }; max-age=${maxAge}; domain=${matchedDomain}; path=/`;
  }
};

const parseCookie = (pkey: string, hasEncode: boolean = true) => {
  let result = null;

  if (!isBrowser) {
    return result;
  }

  try {
    const cookie = getDocumentCookiesObfuscated();
    const matches = String(cookie).match(new RegExp(`anue.${pkey}\=[^;]+`));

    if (matches) {
      // split anue.sso=[.....]
      let json: string = String(matches[0]).split('=')[1];
      if (hasEncode) {
        json = defuscate(atob(String(json)));
      }
      // parse the string as JSON
      result = JSON.parse(json);
    }
  } catch (ignored) {}

  return result;
};

const sso = {
  updateSSOCookie: (data: any) => {
    sso.setSSOCookie(data);
  },
  setSSOCookie: (token: string) => {
    createCookie('sso', token, 315360000);
    console.log('[SSO] setSSOCookie %o', token);
  },
  getAuthFlag: (): string | null => {
    return parseCookie('ssof');
  },
  setAuthFlag: (flag: string) => {
    createCookie('ssof', flag, 315360000);
  },
  getSSOFlag: (): string | null => {
    return parseCookie('ssof');
  },
  getSSOCookie: (): string | null => {
    return parseCookie('sso');
  },
  getCachedUser: (): {
    cognitoId: string;
    profile: Anue.Auth.UserProfile;
    time: number;
  } | null => {
    return parseCookie('ssoc');
  },
  cacheValidUser: (
    cognitoId: string,
    profile: Anue.Auth.UserProfile,
    ttl: number = 300
  ) => {
    createCookie(
      'ssoc',
      {
        cognitoId,
        profile: {
          avatar: profile.avatar,
          name: profile.name,
          email: profile.name
        }
      },
      ttl
    );
  },
  removeCachedContext: () => {
    createCookie('sso.express', {}, 0, false);
  },
  getCachedContext: (): { token: string; provider: string } | null => {
    return parseCookie('sso.express', false);
  },
  cacheContext: (
    ctx: { token: string; provider: string },
    ttl: number = 86400000
  ) => {
    createCookie('sso.express', ctx, ttl, false);
  },
  removeCachedUser: () => {
    createCookie('ssoc', {}, 0);
  },
  removeSSOCookie: () => {
    if (matchedDomain) {
      ['sso', 'ssoc', 'sso.express', 'ssof', 'ssoem'].forEach(cookie =>
        createCookie(cookie, {}, 0)
      );
    }
  },
  setEmailUser: (email: string, password: string) => {
    createCookie('ssoem', [email, password], 315360000);
  },
  getEmailUser: (): [string, string] | null => {
    return parseCookie('ssoem');
  }
};

anue.shared.set('utils.sso', sso);

export default sso;

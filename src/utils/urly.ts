function urly(...args: string[]): string {
  let s = String(args.join('/'));
  const protocol = /[^\:]+\:\/\//.exec(s);
  s = s.replace(/[^\:]+\:\/\//, '');
  if (protocol) {
    return protocol[0] + s.replace(/\/+/g, '/');
  }
  return s.replace(/\/+/g, '/');
}

anue.shared.set('utils.urly', urly);

export default urly;

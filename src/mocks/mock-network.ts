import * as Network from '@libraries/Network';
import waitty from '@utils/waitty';

export const requestSpy = jest.fn();

class NetworkMock extends Network.AbstractNetworkInterface {
  private mocks: Array<{
    test: (opt: Anue.Network.RequestOptions) => boolean;
    responder: (
      opt: Anue.Network.RequestOptions,
      extraRef: any
    ) => Promise<any>;
    delay?: number;
  }> = [];

  clearMocks = () => {
    this.mocks = [];
  };

  /**
   * Mock response of specific tester, return a function which removes the mock
   * @param test A function tests the request options, use the mock if tester returns true
   * @param responder Mock responder function, can returning data or either throwing error
   * @param delay A number which helps to simulate network latency
   * @returns Function which removes the mock
   */
  mock = (
    test: (opt: Anue.Network.RequestOptions) => boolean,
    responder: (opt: Anue.Network.RequestOptions, extraRef: any) => any,
    delay?: number
  ) => {
    const mock = { test, responder, delay };
    this.mocks.push(mock);
    return async () => {
      this.mocks.splice(this.mocks.indexOf(mock), 1);
    };
  };

  protected request = async <T = any>(
    options: Anue.Network.RequestOptions,
    extraRef: any
  ): Promise<T> => {
    requestSpy(options);
    for (const mock of this.mocks) {
      if (mock.test(options)) {
        if (mock.delay) {
          console.log('[mock] wait for %o ms', mock.delay);
          await waitty(mock.delay);
        }
        return await mock.responder(options, extraRef);
      }
    }
    // @ts-ignore
    return await {};
  };
}

const inst = new NetworkMock();

export default inst;

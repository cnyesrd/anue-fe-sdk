let cookies: { provider: string; accessToken?: string } | null = null
let ssoc: { cognitoId: string, profile: Anue.Auth.UserProfile, time: number } | null = null

const sso = {
  updateSSOCookie: (data: any, domain) => {
    const original = sso.getSSOCookie()
    sso.setSSOCookie({
      original, ...data
    }, domain)
  },
  setSSOCookie: (data: { provider: string; accessToken: string }, domain) => {
    cookies = data
  },
  getSSOCookie: (): { provider: string; accessToken?: string } | null => {
    return cookies
  },
  getCachedUser: (): { cognitoId: string, profile: Anue.Auth.UserProfile, time: number } | null => {
    return ssoc
  },
  cacheValidUser: (cognitoId: string, profile: Anue.Auth.UserProfile) => {

    ssoc = { cognitoId, profile, time: Date.now() }

  },
  removeCachedUser: () => {
    ssoc = null
  },
  removeSSOCookie: () => {
    cookies = null
  }
}

export default sso

let emitHandles = {};
let onHandles = {};

export const reset = () => {
  emitHandles = {};
  onHandles = {};
};

export const handleEmitType = (type, fn) => {
  emitHandles[type] = fn;
  return () => (emitHandles[type] = undefined);
};
export const handleOnType = (type, fn) => {
  onHandles[type] = fn;
  return () => (onHandles[type] = undefined);
};

export default class MockEventy {
  emit = (type, ...args) => {
    if (emitHandles[type]) {
      emitHandles[type](...args);
    }
  };
  on = (type, fn) => {
    if (onHandles[type]) {
      onHandles[type](fn);
    }
  };
}

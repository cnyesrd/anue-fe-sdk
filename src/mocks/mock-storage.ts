import * as Storage from '@libraries/Storage';

const storage = {};

class StorageMock extends Storage.AbstractStorageInterface {

  protected removeItemImpl = jest.fn().mockImplementation(
    async (key: string | number): Promise<boolean> => {
      delete storage[key];
      return true;
    }
  );

  protected setItemImpl = async (
    key: string | number,
    value: any
  ): Promise<boolean> => {
    storage[key] = value;
    return true;
  };

  protected getItemImpl = async (key: string | number): Promise<string> => {
    return storage[key];
  };
}

export default new StorageMock();

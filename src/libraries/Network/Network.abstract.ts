/**
 * @doc
 * @anue-library Anue.AbstractNetworkInterface
 *
 * ## 概述
 *
 * 這個章節將會介紹在SDK中的網路層的運作方式。
 * 在SDK中所有的網路存取功能都會使用[Network.getDriver](->Anue.Network#getDriver)的方式來取得一個實體，
 * 在一般情境下，應用端在載入適當的`preset`之後就能夠取得對應於當前環境的Network實作
 *
 * ```Javascript
 * import 'anue-fe-sdk'
 * // 在這個階段就會載入實作
 * import 'anue-fe-sdk/web-preset'
 * ```
 *
 * 如果我們打開`web-preset`的實作來看，可以看到在preset裡面透過`Fetch`實作了一個Network driver
 * 並且透過[setDriver](->Anue.Network#setDriver)將他設定為我們接著要使用的實作。
 *
 * ```Javascript
 * import FetchNetworkDriver from './FetchNetworkDriver';
 * import AsyncStorageDriver from './AsyncStorageDriver';
 *
 * SharedContext.set(Keys.Storage.Driver, new AsyncStorageDriver());
 * SharedContext.set(Keys.Network.Driver, new FetchNetworkDriver());
 *
 * export { Storage, Network };
 *
 * ```
 * ## 參考
 */

/**
 * @doc
 * @class AbstractNetworkInterface
 *
 * 網路層實作的抽象類別，所有網路相關的功能都在這個架構上運作，這之中提供了一些擴充的方法如：
 * - [addRequestInterceptor](#addRequestInterceptor) 在請求送出之前擴充options的結構或進行額外請求
 * - [addResponseInterceptor](#addResponseInterceptor) 在請求回應之後延後或擴充回應的內容（如[Auth](->Anue.Auth)處理__401 Unauthorized__的狀況）
 */

const SystemHeadersMemo: Record<string, Record<string, string>> = {};
const HostToAppNameMap = {
  'stock-dev': 'stock',
  stock: 'stock',
  'global-stock': 'stock'
};
const HostToSystemKindMap = {
  'stock-dev': 'STOCK',
  stock: 'STOCK',
  'global-stock': 'STOCK',
  'global-stock-dev': 'STOCK',
  login: 'LOGIN',
  'login-dev': 'LOGIN'
};

function getSystemKindAndPlatformHeaders() {
  if (typeof location === 'undefined') {
    return {
      'X-System-Kind': 'MEDIA',
      'X-Platform': 'WEB',
      'X-CNYES-APP': 'WEB'
    };
  }
  if (SystemHeadersMemo[location.hostname]) {
    return SystemHeadersMemo[location.hostname];
  }
  const firstPart = location.hostname.split('.')[0];
  const overrides = typeof anue === 'undefined'
    ? {}
    : (anue['net:x-headers-overrides'] || {})
  const result = {
    'X-System-Kind': HostToSystemKindMap[firstPart] || 'UNKNOWN',
    'X-Platform': 'WEB',
    'X-CNYES-APP': HostToAppNameMap[firstPart] || 'unknown',
    ...overrides
  };
  SystemHeadersMemo[location.hostname] = result;

  return result;
}

abstract class AbstractNetworkInterface {
  static onRequestInterceptors: Anue.Network.RequestInterceptor[] = [];
  private static onResponseInterceptors: Anue.Network.ResponseInterceptor[] = [];
  protected baseUrl: string = '';

  /**
   * @doc
   * @method request
   * @param options [fn:expand_fragment](RequestOptions:type-request-options)型別的物件
   * @abstract
   * @protected
   * @promise
   * 一般來說在實作一個新的`Driver`時，這是唯一需要實作的方法，實作上只要是一個接受[fn:expand_fragment](RequestOptions:type-request-options)並透過`Promise`返回
   * [fn:expand_fragment](RequestOptions:type-request-options)的函數即可。
   */
  protected abstract request<T = any>(
    options: Anue.Network.RequestOptions<T>,
    extraRef: any
  ): Promise<T>;

  static clearInterceptors = () => {
    AbstractNetworkInterface.onRequestInterceptors = [];
    AbstractNetworkInterface.onResponseInterceptors = [];
  }

  /**
   * @doc
   * @method addRequestInterceptor
   * @param fn:Function 用於處理回應請求的[fn:expand_fragment](NetworkInterceptor:network-interceptor)函數
   * @returns Function 呼叫這個函數來移除加入的Interceptor
   * @static
   * @public
   * 加入一個__Interceptor__於當Network的請求處理佇列中，這個方法會返回一個可以將這個__Interceptor__移除的函數
   */
  static addRequestInterceptor = (fn: Anue.Network.RequestInterceptor) => {
    AbstractNetworkInterface.onRequestInterceptors.push(fn);
    return () => {
      const index = AbstractNetworkInterface.onRequestInterceptors.indexOf(fn);
      if (index > -1) {
        AbstractNetworkInterface.onRequestInterceptors.splice(index, 1);
      }
      // @ts-ignore
      fn = null;
    };
  };

  /**
   * @doc
   * @method addResponseInterceptor
   * @param fn:Function 用於處理回應請求的[fn:expand_fragment](NetworkResponseInterceptor:network-response-interceptor)函數
   * @returns Function 呼叫這個函數來移除加入的Interceptor
   * @static
   * @public
   * 加入一個__Interceptor__於當Network的回應處理佇列中，這個方法會返回一個可以將這個__Interceptor__移除的函數
   */
  static addResponseInterceptor = (fn: Anue.Network.ResponseInterceptor) => {
    AbstractNetworkInterface.onResponseInterceptors.push(fn);

    return () => {
      const index = AbstractNetworkInterface.onResponseInterceptors.indexOf(fn);
      if (index > -1) {
        AbstractNetworkInterface.onResponseInterceptors.splice(index, 1);
      }
      // @ts-ignore
      fn = null;
    };
  };

  /**
   * @doc
   * @method setBaseUrl
   * @param url 基礎URL的值，必須為字串
   * @returns this
   * @public
   * 設定當前的`Driver`實體所使用的預設__host url__，預設值是`/`
   */
  setBaseUrl = (url: string) => {
    this.baseUrl = url;
    return this;
  };

  send = async <T = any>(
    options: Anue.Network.RequestOptions<T>
  ): Promise<T> => {
    const {
      onRequestInterceptors,
      onResponseInterceptors
    } = AbstractNetworkInterface;
    options.headers = {
      ...getSystemKindAndPlatformHeaders(),
      ...(options.headers || {})
    };

    let extendedOptions = { ...options };
    extendedOptions.url = this.baseUrl + options.url;
    // run request interceptors
    for (const f of onRequestInterceptors) {
      extendedOptions = await f(extendedOptions);
    }
    const extra = {};
    let extendedResult = await this.request<T>(extendedOptions, extra);

    if (Array.isArray(options.handles)) {
      options.handles.forEach(h => {
        extendedResult = h(extendedOptions, extendedResult) || extendedResult;
      });
    }
    // run response interceptors
    for (const f of onResponseInterceptors) {
      extendedResult = await f(extendedOptions, extendedResult, extra);
    }

    return await extendedResult;
  };
}

anue.shared.set('library.net.abstract', AbstractNetworkInterface);

export type Abstract = typeof AbstractNetworkInterface;
export default AbstractNetworkInterface;

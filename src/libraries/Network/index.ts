/**
 * @doc
 * @anue-base Anue.Network
 * 這個命名空間主要包含了SDK中網路存取相關的API，由於考量到需要讓SDK在不同的環境上運作，
 * 在這邊開發者可以實作[AbstractNetworkInterface](->Anue.Network.AbstractNetworkInterface)抽象介面已讓SDK在不同的環境下運作。
 *
 * 相關條例:
 *
 * - 網路抽象層架構解析與實作說明
 * - 加入網路中介(middleware)
 *
 * 在預設的環境下SDK使用[FetchNetworkDriver](->Anue.Network.FetchNetworkDriver) 的實作來處理網路存取
 */
const Shared = anue.shared;
const Keys = Shared.get('constants.keys');
const BaseNetwork = Shared.get('library.net.abstract');
/**
 * @doc
 * @method setDriver
 * @param driver An driver which extends NetworkInterface
 * @static
 * @public
 * 呼叫這個方法並傳入一個 [AbstractNetwork](->Anue.Network.AbstractNetwork) 的時例以替代現有的網路介面實例，一個應用情境：
 *
 * 當我們在`Jest`環境上撰寫單元測試候經常會需要模擬伺服器的回應，或許我們可以啟動一個測試用
 * 的HTTP伺服器、抑或是mock現有的網路層。然而，要是我們可以替換網路層的實作，那麼這件事情
 * 就會變得相對簡單。
 *
 * 我們可以實作一個很明確的`MockNetwork`類別：
 *
 * ```Javascript
 * class MockNetwork extends AbstractNetworkInterface {
 *
 *   request = jest.fn()
 *    .mockImplementation(async (options: Anue.Network.RequestOptions): Promise<Anue.Network.Response> => {
 *      // mock here
 *   });
 *
 * }
 *
 * Network.setDriver(new MockNetwork())
 * ```
 * 接著我們只要將測試用的伺服器回應實作在`reuqest`方法中，再呼叫`setDriver`，即可開始進行測試
 * 並且這樣的行為可以在別的測試情境中輕易的被移除，實作上也更加清楚。
 *
 * 指定的driver實體將存在 [SharedContext](->Anue.SharedContext) 裡面，以便讓不同的Package存取。
 */
const setDriver = (driver: Anue.Network.AbstractNetworkInterface) => {
  anue.shared.set(Keys.Network.Driver, driver);
};

/**
 * @doc
 * @method getDriver
 * @static
 * @public
 * 取得當前的Network實體，通常在開發SDK相關的功能時會使用到
 *
 * ```javascript
 * import * as Network from 'anue-fe-sdk'
 *
 * const net = Network.getDriver()
 *
 * // 向URL發出一個HTTP的請求
 * net.request({ url, headers })
 * ```
 */
const getDriver = (): Anue.Network.AbstractNetworkInterface => {
  return anue.shared.get<Anue.Network.AbstractNetworkInterface>(
    Keys.Network.Driver
  );
};

const addRequestInspector = BaseNetwork.addRequestInterceptor;
const addResponseInspector = BaseNetwork.addResponseInterceptor;

const net = {
  BaseNetwork,
  setDriver,
  getDriver,
  clearInterceptors: BaseNetwork.clearInterceptors,
  addRequestInterceptor: addRequestInspector,
  addResponseInterceptor: addResponseInspector
};

anue.shared.set('library.net', net);

export {
  BaseNetwork as AbstractNetworkInterface,
  setDriver,
  getDriver,
  addResponseInspector,
  addRequestInspector
};

/**
 * @doc
 * @anue-base Anue.Storage
 * TODO
 */

import Keys from '@constants/Keys';
import AbstractStorageInterface from '@libraries/Storage/Storage.abstract';
import SharedContext from '@utils/shared';

/**
 * Set another driver instance to replace the existing one.
 * Default driver is Fetch-implemented one
 * @param driver An driver which extends NetworkInterface
 */
const setDriver = (driver: AbstractStorageInterface) => {
  return SharedContext.set(Keys.Storage.Driver, driver);
};

const getDriver = (): AbstractStorageInterface => {
  return SharedContext.get<AbstractStorageInterface>(Keys.Storage.Driver);
};

export { setDriver, getDriver, AbstractStorageInterface };

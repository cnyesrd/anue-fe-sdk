import * as Storage from '@libraries/Storage';

describe('Storage tests', () => {
  class MockStorage extends Storage.AbstractStorageInterface {
    storage = {};

    protected removeItemImpl = jest.fn().mockImplementation(
      async (key: string | number): Promise<boolean> => {
        delete this.storage[key];
        return true;
      }
    );

    setItemImpl = jest.fn().mockImplementation(
      async (key: string, value: any): Promise<boolean> => {
        this.storage[key] = value;
        return true;
      }
    );

    getItemImpl = jest.fn().mockImplementation(
      async (key: string): Promise<string> => {
        return JSON.stringify(this.storage[key]);
      }
    );
  }

  const driver = new MockStorage();

  Storage.setDriver(driver);
  const storage = Storage.getDriver();

  it('set driver work as expected', () => {
    expect(Storage.getDriver() instanceof MockStorage).toBe(true);
  });

  it('set and get functionalities are fine', async () => {
    await storage.setItem('test', 100);
    expect(await storage.getItem('test')).toBe('100');

    await storage.setItem('an object', { foo: 'bar' });
    expect(await storage.getItem('an object')).toBe(
      JSON.stringify({ foo: 'bar' })
    );

    await storage.setItem('a function', () => {});
    expect(await storage.getItem('a function')).toBeUndefined();

    await storage.setItem(13123123, () => {});
    expect(await storage.getItem(13123123)).toBeUndefined();
  });

  it('when key is not a string, throwing error', () => {
    [null, undefined, {}, () => {}, NaN]
      .map(key => {
        // @ts-ignore
        expect(storage.setItem(key, 1)).toThrowErrorMatchingSnapshot();
        return key;
      })
      .forEach(key => {
        // @ts-ignore
        expect(storage.getItem(key)).toThrowErrorMatchingSnapshot();
      });
  });
});

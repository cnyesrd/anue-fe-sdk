global.DEBUG = false;

require('@utils/shared');
require('@constants/Keys');
require('@constants/Endpoints');
require('@utils/waitty');
require('@utils/getty');
require('@utils/eventy');
require('@utils/rpc');
require('@utils/res');
require('@utils/obfuscator');
require('@utils/paramly');
require('@utils/sso');
require('@utils/urly');
require('@utils/urld');
require('@utils/res');
require('@libraries/Network/Network.abstract');
const Network = require('@libraries/Network');
const MockNetwork = require('./src/mocks/mock-network').default;

Network.setDriver(MockNetwork);

require('@libraries/Storage');

require('@packages/OTAC');
require('@packages/IMC');
require('@packages/Auth');

global.mockAs = function mockAs(target) {
  return target;
}

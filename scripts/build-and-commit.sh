VERSION=$1
echo $1
git tag source-$VERSION
git push --tags
# Get origin url of current git
ORIGIN_URL=$(git config --get remote.origin.url)
# build js bundles
yarn run build
# publish pure bundle
cd build
git init
git remote add origin $ORIGIN_URL
cp ../package.json ./
git add . -A
git commit -m "anue-fe-sdk publish commit $VERSION"
git push origin master:release/$VERSION -f
npm publish
cd ..
rm -rf ./build

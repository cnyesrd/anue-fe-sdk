var chalk = require('chalk')
var execSync = require('child_process').execSync
var message = execSync('git log -n 1 --format=%s')

var match = /#v[a-z0-9\.-]+/.exec(message)

if(match) {
  var version = String(match[0]).replace(/[#\s]/g, '')
  console.log(`${chalk.yellow('Found release tag in commit message')}`)
  console.log(`Publish anue-fe-sdk ${chalk.green(version)}`)
  process.stdout.write(execSync('sh ./scripts/build-and-commit.sh ' + version))

}

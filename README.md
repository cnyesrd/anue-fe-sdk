# Anue Fe SDK

SDK development project, `VSCode` is recommended

## Debug

Start webpack and re-compile on files change, by default, the file will write to `./build` folder

```
$ yarn debug
```

## Pipe Output Files

It's pretty common when you're debugging SDK modules in other projects, you probably don't want to
publish a new version to npm every time you make a change.

Instead, you can directly let the packager output files to specific folder(s) by this command

```
$ yarn pipe /path/to/some/project /path/to/another/project ...
```

## Analyze Packages

```
$ yarn analyze
```

## Publish to Bitbucket and NPM

```
yarn publish-npm v${VERSION_CODE}
```
try {
  require.resolve('http-proxy')
}
catch (err) {
  console.log('http-proxy not installed, please install the package via the command')
  console.log('')
  console.log('$ yarn add http-proxy')
  console.log('')
  console.log('and re-execute proxy again')
  process.exit(0)
}


let https = require('https')
let http = require('http')
let httpProxy = require('http-proxy')
let proxy = httpProxy.createProxyServer({
  changeOrigin: true,
  autoRewrite: true,
  followRedirects: true,
  hostRewrite: true,
});

let mappings = {
  'app.beta.cnyes.cool': 'https://localhost:3001',
	'stock.beta.cnyes.cool': 'https://localhost:3008',
  'stock-dev.beta.cnyes.cool': 'https://localhost:3008',
  'global-stock-dev.beta.cnyes.cool': 'httsp://localhost:3007',
  'app.dev.beta.withgod-test.anue.in': 'http://localhost:3000',
  'member-dev.beta.cnyes.cool': 'https://localhost:3005',
	'global-stock-dev.beta.cnyes.cool': 'httsp://localhost:3008',
  'app.dev.int.withgod-test.anue.in': 'http://localhost:3000',
  'login-dev.beta.cnyes.cool': 'https://localhost:9001',
}

let handle = (req, res) => {
  let host = req.headers.host
  let url = host + req.url
  let target = 'https://localhost:9001'

  for (const i in mappings) {

    if (host.startsWith(i)) {
      target = mappings[i]
      break
    }

  }
  console.log('forward %s to => %s', url, target)
  proxy.web(req, res, {
    target,
    secure: false });
  proxy.on('error', (err) => {
    console.log('Cannot forward request to [%s]', url);
    console.log('Please make sure the source server is running.')
  })
}


https.createServer({
  key:
`
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAs5BUumc7/KKfwLb+B7+NP1maKdHE5y3LBwai5mW/Ov1212LS
wLAl5o69tv0r5F/YHqtL3kOWfUjaR9kxqOeY2A12c2vqTlQ+K/hmAht14bxPtvlv
Eqgn+xwSXLYaX+yDWCBA+UX/ao7tw/E5adYipLyeSZTYw5ZWGTb8ad5b06ak8FnY
HaSGqxruPhP8UKpsc+7BYwli9Qs/0/zUQ+OdgkzDpJoP7Ua4TCvQ0YP9XWXhirV/
z0XSz0N1GB533pSxhoTnFhz3zmhWh2h6deexRP1CsE+380R1sv7iJ/5VBM8hi13K
+omWrwUbudpf6Z5NfzDx4W/6H2u2L/yCjk3sAQIDAQABAoIBAHOIXuucdp1DKVR+
qQ3lMoVbKnTT0kQIU8ROgcuKFbNn1+oXXJC8BXbFys0NMgt7m3uILYVxqUcs870c
wvXxJYeFifwTqCsDywjRhxi+AGfgmM2ayduQNEE+O5ZJWMj1TUELZ6K7Ze5yzgqv
j4hITfv+nD2ttpaJiRYqKdgYSK7xSmjtTWFZoVOySZGZkqxTfVYjg4pAGY9Dy+l+
2U0/gPYok+zL1RgLKfGAFTkZ5a46L6RjAcldPldrNyN32NwjKnFOiCxkDrfcKZI+
pYmkZScdsaI6mgCk7eLcXYcN6ijKIvywSqbt4l9sxgER+NvHJPgtwlsRqj41S32s
Nzw8ZAECgYEA5DA/DsUFN4Xu3W9Acfcd9ynNKc55HFaX2vaTKfEx3PO6tqse5THP
tUHQvkBr+uTa486jAfFgVPRpz7smsspr9Z1acfSOt2xRk3nqoxcH0Hb8lZX1qfs6
AoBb4l9+YoaHjww7A8JDHkcL4ctzacyO65g3p2WAB/nuON5FG7UdwuECgYEAyXLv
yHY/y2Z/3pamTAS46B7ellyJHPT0pxYvXzLb0thZa0Gkx2XRft/rKmgn2CqtA65M
jforXA8jHXBzOT/Sgj6g/JusphlNoUCe/BfJ11+Iq5kHpm/d0v6z+Lndopfl11BH
BUVsCVViCP46zPcSvcSktIXhsfOQdRx8nzc+bSECgYEAqWQPS12CM+denE6b3n39
Zp9HR8OYMCsv60GjmT0NMOJVNojtFGLXt8w1syWLguIkg3u/kg+m3Sna6nQZlBuj
7N50Z6eN0ie8nuegAJV2HZBWjIyXoyl1tKmoi/ErluQa+Vuw62dn/6/p6CHpeR56
8fU7Sy6TzlgAOYL2dk0K3MECgYA6wlfGtRcukRtDJdYj3kzBCCAM47jqxViHVCVF
M2SDP6/1ZRRXRQ+yXkYN/IuCtg/5PuH9l7cGBzRK5f31+PPCIdp7VD0rxb2rB1pY
Vef99lO7GcvtdDvTy49bQXuwbXYRM81DcFhu8ITD8sPb/UVuLOmN7m9CpLTCNCj1
l/uRgQKBgQCjdGY+OCGKAgWjsS8X5Cxtv6sz/zDEOpS2c88mTGQTc1P/VblYSrmR
JF71Shm8MFRW+wle0PWZRYMCspyeZgXiHYHTM6gi8td31pqV9dw7hZCuCoaMARZq
teqFtitazHT1QpJeOJaMBQUUu0auwQOxHqZfcM4iA+/9bMw90Z97IQ==
-----END RSA PRIVATE KEY-----

`,
  cert:
`
-----BEGIN CERTIFICATE-----
MIIDcDCCAlgCCQCCo1cj0pvzXzANBgkqhkiG9w0BAQsFADB6MQswCQYDVQQGEwJU
VzELMAkGA1UECAwCdHcxCzAJBgNVBAcMAnR3MQ0wCwYDVQQKDARhbnVlMQ0wCwYD
VQQLDARhbnVlMQ0wCwYDVQQDDARhbnVlMSQwIgYJKoZIhvcNAQkBFhVhbnVlQGFu
dWVncm91cC5jb20udHcwHhcNMTgxMjI1MTAwMzM5WhcNMTkxMjI1MTAwMzM5WjB6
MQswCQYDVQQGEwJUVzELMAkGA1UECAwCdHcxCzAJBgNVBAcMAnR3MQ0wCwYDVQQK
DARhbnVlMQ0wCwYDVQQLDARhbnVlMQ0wCwYDVQQDDARhbnVlMSQwIgYJKoZIhvcN
AQkBFhVhbnVlQGFudWVncm91cC5jb20udHcwggEiMA0GCSqGSIb3DQEBAQUAA4IB
DwAwggEKAoIBAQCzkFS6Zzv8op/Atv4Hv40/WZop0cTnLcsHBqLmZb86/XbXYtLA
sCXmjr22/SvkX9geq0veQ5Z9SNpH2TGo55jYDXZza+pOVD4r+GYCG3XhvE+2+W8S
qCf7HBJcthpf7INYIED5Rf9qju3D8Tlp1iKkvJ5JlNjDllYZNvxp3lvTpqTwWdgd
pIarGu4+E/xQqmxz7sFjCWL1Cz/T/NRD452CTMOkmg/tRrhMK9DRg/1dZeGKtX/P
RdLPQ3UYHnfelLGGhOcWHPfOaFaHaHp157FE/UKwT7fzRHWy/uIn/lUEzyGLXcr6
iZavBRu52l/pnk1/MPHhb/ofa7Yv/IKOTewBAgMBAAEwDQYJKoZIhvcNAQELBQAD
ggEBAK7k84fXAniJoeNT49NjcQRXLukgQ8aggqddLZRZRkftzo+mXpfHSnpS/PFQ
X9pgw/JyGkPmxBhG6vvDbRgGQIKabD7TogCjHdWxZ1tHI4BovVOoM3nwpfBC2KpP
vbc8MAZizcCOgjmw+J+tj6TCHusn58NpGlTlt/8SVq+MM0nX9GPm4NtUpFn8vSKS
Lx3/jCtJ1IyfQejtgsFtVJKIO5I10Bbl7UG8S0K/zCPqpFkgLXIYLBPRxjCLre0/
8/oAvxZubhTbRqrJExAAhAxx2SyPaMAzvJzxeYDSGLZ5r6HkpaoDnOQsQ+5ebs6V
lpMdm3azbU3w0KRwGUEZ2XpHk1s=
-----END CERTIFICATE-----
`
}, handle)
.listen(443);

http.createServer(handle).listen(80);
// do not delete this line
console.log('PROXYOK');
console.log('Forwarding server started');
console.log(mappings)
